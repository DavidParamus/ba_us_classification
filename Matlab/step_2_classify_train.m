%% Clear workspace and console
clc;
%clear;

%% Run date
today = datetime('today');
run_date = datestr(today, 'yyyy-mm-dd');
%run_date = '2021-04-08';

%% Load Images 
inputSize = [224 224 3];

trainDir =fullfile(pwd,'Dataset\simple_20210408\Train_Image_Aug\');
imdsTrain = imageDatastore(trainDir, 'LabelSource', 'foldernames', 'IncludeSubfolders',true);
imdsTrain.ReadFcn = @(loc)imresize(imread(loc),inputSize(1:2));
tbl = countEachLabel(imdsTrain);
validateDir =fullfile(pwd,'Dataset\simple_20210408\Validation_Image\');
imdsValidation = imageDatastore(validateDir, 'LabelSource', 'foldernames', 'IncludeSubfolders',true);
imdsValidation.ReadFcn = @(loc)imresize(imread(loc),inputSize(1:2));

augimdsTrain = augmentedImageDatastore(inputSize,imdsTrain, ...
    'ColorPreprocessing', 'gray2rgb', ...
    'OutputSizeMode','randcrop');
augimdsValidation = augmentedImageDatastore(inputSize, imdsValidation, 'ColorPreprocessing', 'gray2rgb');

%% Training Network Option
miniBatchSize = 25;
learnRate = 0.001;
MaxEpochs = 50;
valFrequency = 20;
options = trainingOptions('sgdm', ...
    'InitialLearnRate',learnRate, ...
    'MaxEpochs',MaxEpochs, ...
    'MiniBatchSize',miniBatchSize, ...
    'Verbose',true,...
    'VerboseFrequency',50, ...
    'Shuffle','every-epoch', ...
    'Plots','training-progress', ...
    'ValidationData', augimdsValidation, ...
    'ValidationFrequency',valFrequency, ...
    'LearnRateSchedule','piecewise', ...
    'LearnRateDropFactor',0.1, ...
    'LearnRateDropPeriod',10);

%% Define and Train Network Architecture - 1 - residualCIFARlgraph
%{
netName_1 = 'simple_1_CIFARNet_20_standard.mat';
numUnits = 12;
netWidth = 16;
lgraph_1 = residualCIFARlgraph(inputSize, size(tbl, 1),netWidth,numUnits,"standard");

trainedNet=trainNetWork(netName_1, augimdsTrain, lgraph_1, options, run_date);
evaluateTrainNetwok(imdsTrain, imdsValidation, trainedNet, netName_1, run_date);
%}

%% Define and Train Network Architecture - 2 - ResNet

netName_2 = 'simple_2_resnet101.mat';
lgraph_2 = getResNet(tbl{:,1}, 'resnet101');

trainedNet=trainNetWork(netName_2, augimdsTrain, lgraph_2, options, run_date);
evaluateTrainNetwok(imdsTrain, imdsValidation, trainedNet, netName_2, run_date);

netName_2_1 = 'simple_2_resnet18.mat';
lgraph_2_1 = getResNet(tbl{:,1}, 'resnet18');

trainedNet=trainNetWork(netName_2_1, augimdsTrain, lgraph_2_1, options, run_date);
evaluateTrainNetwok(imdsTrain, imdsValidation, trainedNet, netName_2_1, run_date);

netName_2_2 = 'simple_2_resnet50.mat';
lgraph_2_2 = getResNet(tbl{:,1}, 'resnet50');

trainedNet=trainNetWork(netName_2_2, augimdsTrain, lgraph_2_2, options, run_date);
evaluateTrainNetwok(imdsTrain, imdsValidation, trainedNet, netName_2_2, run_date);


%% Define and Train Network Architecture - 3 - VGGNet
options.MiniBatchSize = 20;

netName_3 = 'simple_3_vgg16.mat';
lgraph_3 = getVGGNet(tbl{:,1}, 'vgg16');

trainedNet=trainNetWork(netName_3, augimdsTrain, lgraph_3, options, run_date);
evaluateTrainNetwok(imdsTrain, imdsValidation, trainedNet, netName_3, run_date);

netName_3_1 = 'simple_3_vgg19.mat';
lgraph_3_1 = getVGGNet(tbl{:,1}, 'vgg19');

trainedNet=trainNetWork(netName_3_1, augimdsTrain, lgraph_3_1, options, run_date);
evaluateTrainNetwok(imdsTrain, imdsValidation, trainedNet, netName_3_1, run_date);
options.MiniBatchSize = 25;
%}

%% Define and Train Network Architecture - 4 - Shufflenet
netName_4 = 'simple_4_shufflenet.mat';
lgraph_4 = getShufflenet(tbl{:,1});
trainedNet=trainNetWork(netName_4, augimdsTrain, lgraph_4, options, run_date);
evaluateTrainNetwok(imdsTrain, imdsValidation, trainedNet, netName_4, run_date);


%% Define and Train Network Architecture - 5 - GoogleNet
netName_5 = 'simple_5_googlenet.mat';
lgraph_5 = getGoogLeNet(tbl{:,1});
trainedNet=trainNetWork(netName_5, augimdsTrain, lgraph_5, options, run_date);
evaluateTrainNetwok(imdsTrain, imdsValidation, trainedNet, netName_5, run_date);

%% Define and Train Network Architecture - 6 - MoblieNet
netName_6 = 'simple_6_moblienetv2.mat';
lgraph_6 = getGoogLeNet(tbl{:,1});
trainedNet=trainNetWork(netName_6, augimdsTrain, lgraph_6, options, run_date);
evaluateTrainNetwok(imdsTrain, imdsValidation, trainedNet, netName_6, run_date);

%% Define and Train Network Architecture - 7 - DenseNet
netName_7 = 'simple_7_densenet201.mat';
lgraph_7 = getDenseNet(tbl{:,1});
trainedNet=trainNetWork(netName_7, augimdsTrain, lgraph_7, options, run_date);
evaluateTrainNetwok(imdsTrain, imdsValidation, trainedNet, netName_7, run_date);


%% Train New Network 
function trainedNet=trainNetWork(netName, augimdsTrain, lgraph, options, run_date)

result_dir = "Run_Result/";
if ~exist(fullfile(result_dir, run_date, 'Train_Log'), 'dir')
    mkdir(fullfile(result_dir, run_date, 'Train_Log'));
end

if ~exist(fullfile(result_dir, run_date, 'networks'), 'dir')
    mkdir(fullfile(result_dir, run_date, 'networks'));
end

if exist(fullfile(result_dir, run_date, 'networks', netName), 'file')
    fprintf('[INFO] Loading network : %s ...\n',netName);
    load(fullfile(result_dir, run_date, 'networks', netName), 'trainedNet');
else
    fprintf('[INFO] Training network : %s ...\n',netName);
    filename_txt = sprintf("TrainingLog_%s_%s.txt", netName ,run_date);
    diary(fullfile(result_dir, run_date, 'Train_Log', filename_txt));
    diary on;
    trainedNet = trainNetwork(augimdsTrain,lgraph,options);
    diary off;
    networkNames = fullfile(result_dir, run_date, "networks", netName);
    save(networkNames,'trainedNet');
    
    filename_gcf = sprintf("TrainingGcfResult_%s.png", netName);
    training_gcf = findall(groot, 'Type', 'Figure');
    exportapp(training_gcf,fullfile(result_dir, run_date, 'Train_log',filename_gcf));
    close(training_gcf);

end

end

%% Evaluate Train Netwok
function evaluateTrainNetwok(imdsTrain, imdsValidation, trainedNet, netName, run_date)
netName = strrep(netName,"_","-");

result_dir = "Run_Result\";
writeDir = fullfile(result_dir, run_date, 'Evaluation');
if ~exist(writeDir, 'dir')
    mkdir(writeDir);
end

% Evaluate Trained Network
valLabels = imdsValidation.Labels;
[valPred,vprobs] = classify(trainedNet, imdsValidation);
validationError = mean(valPred ~= valLabels);
trainLabels = imdsTrain.Labels;
[trainPred,tprobs] = classify(trainedNet, imdsTrain);
trainError = mean(trainPred ~= trainLabels);
disp("[INFO] Training error: " + trainError*100 + "%")
disp("[INFO] Validation error: " + validationError*100 + "%")

figure('Units','normalized','Position',[0.2 0.2 0.4 0.4]);
cm = confusionchart(valLabels,valPred);
cm.Title = sprintf('Confusion Matrix for Validation Data - %s', netName);
cm.ColumnSummary = 'column-normalized';
cm.RowSummary = 'row-normalized';
%calculate accuracy
accuracy = sum(valPred == valLabels)/numel(valLabels);
disp("[INFO] Accuracy: " + accuracy*100 + "%")

filename = sprintf("ConfusionMatrix_%s.png", netName);
destination = fullfile(writeDir, filename);
saveas(gcf,destination)
close(gcf)


% Display random sample pf nine test images together with predicted classes and the probabilities of those classes 
figure
idx = randperm(size(imdsValidation.Files,1),9);
for i = 1:numel(idx)
    subplot(3,3,i);
    im = imread(string(imdsValidation.Files(idx(i))));
    imshow(im);
    prob = num2str(100*max(vprobs(idx(i),:)),3);
    predClass = char(valPred(idx(i)));
    if valPred(i) == valLabels(i)
        predCorrect='O';
    else
        predCorrect='X';
    end
    title([predClass,', ',prob,'%, ', predCorrect])
end

filename = sprintf("RandValidResult_%s.png", netName);
destination = fullfile(writeDir, filename);
saveas(gcf,destination)
close(gcf)

end