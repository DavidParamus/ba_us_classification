%% Clear console and workspace
clc;
clear;
diary off;
close all;

%% Run Date
today = datetime('today');
run_date = datestr(today, 'yyyy-mm-dd');
run_date = '20210408networks_overall_simple_670image';
%run_date = [run_date, '_overall'];

%% Load Images
inputSize = [224 224 3];

trainDir =fullfile(pwd,'Dataset/square_20210426/Train_Image_Aug/');
imdsTrain = imageDatastore(trainDir, 'LabelSource', 'foldernames', 'IncludeSubfolders',true);
imdsTrain.ReadFcn = @(loc)imresize(imread(loc),inputSize(1:2));
tbl = countEachLabel(imdsTrain);

testDir =fullfile(pwd,'Dataset/simple_670image/Test_image/');
imdsTest = imageDatastore(testDir, 'LabelSource', 'foldernames', 'IncludeSubfolders',true);
imdsTest.ReadFcn = @(loc)imresize(imread(loc),inputSize(1:2));

writeDir = fullfile(pwd,'Run_Result', run_date, 'Test_Result');
if ~exist(writeDir, 'dir')
    mkdir(writeDir)
end

sheet_idx = 1;
warning( 'off', 'MATLAB:xlswrite:AddSheet' ) ;

filename = sprintf("TestResultLog_%s.txt" ,run_date);
diary(fullfile(writeDir, filename));
diary on;

num_folds = 20;
%{
%% Load and Test Network Architecture - 1 - residualCIFARlgraph
%{
rocTables
row_datas = table();
avgConfusion = zeros(2);
netName_1 = 'simple_1_CIFARNet_20_standard.mat';
fprintf("[INFO] Loading network ""%s"" ...\n", netName_1);
load(fullfile('networks', netName_1), 'netTransfer');
[row_data, rocTables, avgConfusion] = testTrainNetwok(i, num_folds, imdsTrain ,imdsTest, netTransfer, netName_1, run_date, rocTables, avgConfusion);
row_datas = [row_datas; row_data];
%}

%% Load and Test Network Architecture - 2 - ResNet
rocTables = table();
row_datas = table();
avgConfusion = zeros(2);
for i=1:num_folds
    netName_2 = sprintf('resnet18_%d_among_10_folds.mat', i);
    fprintf("[INFO] Loading network ""%s"" ...\n", netName_2);
    load(fullfile('networks',netName_2), 'netTransfer');
    [row_data, rocTables, avgConfusion] = testTrainNetwok(i, num_folds, imdsTrain ,imdsTest, netTransfer, netName_2, run_date, rocTables, avgConfusion);
    row_datas = [row_datas; row_data];
end
writetable(row_datas, fullfile(writeDir, 'Overall_DataSetMetrics.xlsx'), 'Sheet', sheet_idx);
sheet_idx = sheet_idx + 1;

rocTables = table();
row_datas = table();
for i=1:num_folds
    netName_2 = sprintf('resnet50_%d_among_10_folds.mat', i);
    fprintf("[INFO] Loading network ""%s"" ...\n", netName_2);
    load(fullfile('networks',netName_2), 'netTransfer');
    [row_data, rocTables, avgConfusion] = testTrainNetwok(i, num_folds, imdsTrain ,imdsTest, netTransfer, netName_2, run_date, rocTables, avgConfusion);
    row_datas = [row_datas; row_data];
end 
writetable(row_datas, fullfile(writeDir, 'Overall_DataSetMetrics.xlsx'), 'Sheet', sheet_idx);
sheet_idx = sheet_idx + 1;

rocTables = table();
row_datas = table();
for i=1:num_folds
    netName_2 = sprintf('resnet101_%d_among_10_folds.mat', i);
    fprintf("[INFO] Loading network ""%s"" ...\n", netName_2);
    load(fullfile('networks',netName_2), 'netTransfer');
    [row_data, rocTables, avgConfusion] = testTrainNetwok(i, num_folds, imdsTrain ,imdsTest, netTransfer, netName_2, run_date, rocTables, avgConfusion);
    row_datas = [row_datas; row_data];
end 
writetable(row_datas, fullfile(writeDir, 'Overall_DataSetMetrics.xlsx'), 'Sheet', sheet_idx);
sheet_idx = sheet_idx + 1;

%% Load and Test Network Architecture - 3 - VGGNet
rocTables = table();
row_datas = table();
for i=1:num_folds
    netName_3 = sprintf('vgg16_%d_among_10_folds.mat', i);
    fprintf("[INFO] Loading network ""%s"" ...\n", netName_3);
    load(fullfile('networks',netName_3), 'netTransfer');
    [row_data, rocTables, avgConfusion] = testTrainNetwok(i, num_folds, imdsTrain ,imdsTest, netTransfer, netName_3, run_date, rocTables, avgConfusion);
    row_datas = [row_datas; row_data];
end
writetable(row_datas, fullfile(writeDir, 'Overall_DataSetMetrics.xlsx'), 'Sheet', sheet_idx);
sheet_idx = sheet_idx + 1;

rocTables = table();
row_datas = table();
for i=1:num_folds
    netName_3 = sprintf('vgg19_%d_among_10_folds.mat', i);
    fprintf("[INFO] Loading network ""%s"" ...\n", netName_3);
    load(fullfile('networks',netName_3), 'netTransfer');
    [row_data, rocTables, avgConfusion] = testTrainNetwok(i, num_folds, imdsTrain ,imdsTest, netTransfer, netName_3, run_date, rocTables, avgConfusion);
    row_datas = [row_datas; row_data];
end
writetable(row_datas, fullfile(writeDir, 'Overall_DataSetMetrics.xlsx'), 'Sheet', sheet_idx);
sheet_idx = sheet_idx + 1;

%% Load and Test Network Architecture - 4 - Shufflenet
rocTables = table();
row_datas = table();
for i=1:num_folds
    netName_4 = sprintf('shufflenet_%d_among_10_folds.mat', i);
    fprintf("[INFO] Loading network ""%s"" ...\n", netName_4);
    load(fullfile('networks',netName_4), 'netTransfer');
    [row_data, rocTables, avgConfusion] = testTrainNetwok(i, num_folds, imdsTrain ,imdsTest, netTransfer, netName_4, run_date, rocTables, avgConfusion);
    row_datas = [row_datas; row_data];
end 
writetable(row_datas, fullfile(writeDir, 'Overall_DataSetMetrics.xlsx'), 'Sheet', sheet_idx);
sheet_idx = sheet_idx + 1;

%% Load and Test Network Architecture - 5 - GoogleNet
rocTables = table();
row_datas = table();
for i=1:num_folds
    netName_5 = sprintf('googlenet_%d_among_10_folds.mat', i);
    fprintf("[INFO] Loading network ""%s"" ...\n", netName_5);
    load(fullfile('networks',netName_5), 'netTransfer');
    [row_data, rocTables, avgConfusion] = testTrainNetwok(i, num_folds, imdsTrain ,imdsTest, netTransfer, netName_5, run_date, rocTables, avgConfusion);
    row_datas = [row_datas; row_data];
end 
writetable(row_datas, fullfile(writeDir, 'Overall_DataSetMetrics.xlsx'), 'Sheet', sheet_idx);
sheet_idx = sheet_idx + 1;


%% Load and Test Network Architecture - 6 - MoblieNet
rocTables = table();
row_datas = table();
for i=1:num_folds
    netName_6 = sprintf('moblienetv2_%d_among_10_folds.mat', i);
    fprintf("[INFO] Loading network ""%s"" ...\n", netName_6);
    load(fullfile('networks',netName_6), 'netTransfer');
    [row_data, rocTables, avgConfusion] = testTrainNetwok(i, num_folds, imdsTrain ,imdsTest, netTransfer, netName_6, run_date, rocTables, avgConfusion);
    row_datas = [row_datas; row_data];
end 
writetable(row_datas, fullfile(writeDir, 'Overall_DataSetMetrics.xlsx'), 'Sheet', sheet_idx);
sheet_idx = sheet_idx + 1;

%% Load and Test Network Architecture - 7 - DenseNet
rocTables = table();
row_datas = table();
for i=1:num_folds
    netName_7 = sprintf('densenet201_%d_among_10_folds.mat', i);
    fprintf("[INFO] Loading network ""%s"" ...\n", netName_7);
    load(fullfile('networks',netName_7), 'netTransfer');
    [row_data, rocTables, avgConfusion] = testTrainNetwok(i, num_folds, imdsTrain ,imdsTest, netTransfer, netName_7, run_date, rocTables, avgConfusion);
    row_datas = [row_datas; row_data];
end 
writetable(row_datas, fullfile(writeDir, 'Overall_DataSetMetrics.xlsx'), 'Sheet', sheet_idx);
sheet_idx = sheet_idx + 1;


%}
%% Single Networks
netNames = [ 
   "simple_1_CIFARNet_20_standard.mat"
   "simple_2_resnet18.mat"
   "simple_2_resnet50.mat"
   "simple_2_resnet101.mat"
   "simple_3_vgg16.mat"
   "simple_3_vgg19.mat"
   "simple_4_shufflenet.mat"
   "simple_5_googlenet.mat"
   "simple_6_moblienetv2.mat"
   "simple_7_densenet201.mat"
];

rocTables = table();
row_datas = table();
avgConfusion = zeros(2);
for i=1:size(netNames)
    netName = netNames(i);
    fprintf("[INFO] Loading network ""%s"" ...\n", netName);
    load(fullfile('networks','networks_2021-04-08' ,netName), 'trainedNet');
    [row_data, rocTables, avgConfusion] = testTrainNetwok(i, num_folds, imdsTrain ,imdsTest, trainedNet, netName, run_date, rocTables, avgConfusion);
    row_datas = [row_datas; row_data];
end
writetable(row_datas, fullfile(writeDir, 'Overall_DataSetMetrics.xlsx'), 'Sheet', sheet_idx);
sheet_idx = sheet_idx + 1;


%% End of testing
diary off;

%% TestTrainNetwok
function [row_data, rocTables, avgConfusion]=testTrainNetwok(fold_idx, num_folds, imdsTrain, imdsTest, trainedNet, netName, run_date, rocTables, avgConfusion)
    % Get network architecture name
    [~, netNOnly, ~] = fileparts(netName);
    strArray = split(netNOnly, '_');
    netNOnly = strArray{3};
    
    % Evaluate Trained Network
    writeDir = char(fullfile(pwd,'Run_Result', run_date, 'Test_Result', netNOnly));
    if ~exist(writeDir, 'dir')
        mkdir(writeDir);
    end

    % Evaluate Trained Network
    testLabels = imdsTest.Labels;
    tic
    [testPred,test_scores] = classify(trainedNet, imdsTest, 'MiniBatchSize', 50);
    runTime = toc;
    fprintf('[INFO] Overall test images executive time: %f sec\n', runTime);

    rocTable = table(testLabels(:,1), test_scores(:,1), test_scores(:,2),'VariableNames' ,{'Labels','have', 'none'});
    rocTables = [rocTables; rocTable];
    %{
    trainLabels = imdsTrain.Labels;
    [trainPred,train_scores] = classify(trainedNet, imdsTrain, 'MiniBatchSize', 50);
    trainError = mean(trainPred ~= trainLabels);
    %disp("[INFO] Training error: " + trainError*100 + "%");
    %}
    %{
    netSplit = strsplit(netName,"_");
    netNOnly = netSplit(1);
    [~, netNOnly] = fileparts(netNOnly); 
    %}

    % Calculate result
    confusionMat = confusionmat(testLabels, testPred);
    accuracy = sum(testPred == testLabels)/numel(testLabels);
    %accuracy = (confusionMat(1,1) + confusionMat(2,2))  / sum(confusionMat(:));
    precision = confusionMat(1,1) / (confusionMat(1,1) + confusionMat(2,1));
    sensitivity = confusionMat(1,1) / (confusionMat(1,1) + confusionMat(1,2));
    specificity = confusionMat(2,2) / (confusionMat(2,1) + confusionMat(2,2));
    falsePositiveRate = confusionMat(2,1) / (confusionMat(2,1) + confusionMat(2,2));
    falseNegativeRate = confusionMat(1,2) / (confusionMat(1,1) + confusionMat(1,2));
    f1score = 2 / ( ( 1 / precision ) + (1 / sensitivity) );
    fprintf("[INFO] Accuracy: %f%%\n", accuracy*100);
    fprintf("[INFO] Precision: %f%%\n", precision*100);
    fprintf("[INFO] Sensitivity: %f%%\n", sensitivity*100);
    fprintf("[INFO] Specificity: %f%%\n", specificity*100);
    fprintf("[INFO] False Positive Rate: %f%%\n", falsePositiveRate*100);
    fprintf("[INFO] False Negative Rate: %f%%\n", falseNegativeRate*100);
    fprintf("[INFO] F1-Measure: %f%%\n", f1score*100);
    row_data = table({netName}, accuracy, precision, sensitivity, specificity, falsePositiveRate, ...
                     falseNegativeRate, f1score, runTime, ... 
                     'VariableNames',{'Network Name','Accuracy', 'Precision', 'Sensitivity', 'Specificity',...
                                      'False Positive Rate', 'False Negative Rate', 'F1-Score', 'Runtime'});
    

    % Plot Confusion Matrix
    figure('Units','normalized','Position',[0.2 0.2 0.4 0.4]);
    cm = confusionchart(testLabels,testPred, 'FontSize',24);
    cm.Title = 'Confusion Matrix for Test Data';
    %cm.ColumnSummary = 'column-normalized';
    %cm.RowSummary = 'row-normalized';
    filename = sprintf("ConfusionMatrix_Test_%s.png", netName);
    destination = fullfile(writeDir, filename);
    saveas(gcf,destination)
    close(gcf)
    
    if fold_idx == 1
        avgConfusion = confusionMat;
    elseif fold_idx == num_folds
        avgConfusion = avgConfusion / num_folds;
        figure('Units','normalized','Position',[0.2 0.2 0.4 0.6]);
        h = heatmap(['have'; 'none'],['have'; 'none'], avgConfusion);
        h.Title = 'Confusion Matrix for Overall Average Test Data';
        h.XLabel = 'Predicted Class';
        h.YLabel = 'True Class';
        h.FontSize = 24;
        filename = sprintf("ConfusionMatrix_Test_Overall_%s.png", netName);
        destination = fullfile(writeDir, filename);
        saveas(gcf,destination)
        close(gcf)
    else
        avgConfusion = avgConfusion + confusionMat;
    end

    % Display random sample pf nine test images together with predicted classes and the probabilities of those classes
    %{
    figure;
    img_index = 1;
    for i = 1:size(imdsTest.Files,1)
        if mod(i,25) == 0
            subplot(5,5, 25);
        else
            subplot(5,5, mod(i,25));
        end    

        im = imread(string(imdsTest.Files(i)));
        imshow(im);
        prob = num2str(100*max(test_scores(i,:)),3);
        predClass = char(testPred(i));

        %fprintf("[DEBUG] %s vs. %s : %d\n", testPred(i), testLabels(i), testPred(i) == testLabels(i) );
        if testPred(i) == testLabels(i)
            predCorrect='O';
        else
            predCorrect='X';
        end

        title([predClass,', ',prob,'%, ', predCorrect])
        %xlabel(predCorrect)

        if mod(i, 25) == 0
            filename = sprintf("TestResult_%s_%d.png", netName, img_index);
            if ~exist(fullfile(writeDir, netName), 'dir')
                mkdir(fullfile(writeDir, netName));
            end
            destination = fullfile(writeDir, netName, filename);
            saveas(gcf,destination);
            close(gcf);
            img_index = img_index + 1;
            figure;
        end

    end

    filename = sprintf("TestResult_%s_%d.png", netName, img_index);
    destination = fullfile(writeDir, netName, filename);
    saveas(gcf,destination);
    close(gcf);
    %}

    % ROC Curve
    figure(1);
    [x1,y1,~,auc1] = perfcurve(testLabels,test_scores(:,1),'have');
    %[x2,y2,~,auc2] = perfcurve(testLabels,test_scores(:,2),'none');

    plot(x1,y1);
    %hold on;
    %plot(x2,y2);
    %grid;
    xlabel('False positive rate');
    ylabel('True positive rate');
    %legend('have', 'none');
    %title(sprintf("%s (have:%5.2f%% / none:%5.2f%%)",strrep(netName,'_','\_'),auc1*100, auc2*100));
    title(sprintf("%s - BA Patients (AUC:%5.2f%%)",strrep(netNOnly,'_','\_'),auc1 *100));
    filename = sprintf("ROC_Curve_%s.png", netName);
    destination = fullfile(writeDir, filename);
    saveas(gcf,destination);
    close(gcf);
    
    if fold_idx == num_folds
        figure(3);
        [x1,y1,~,auc1] = perfcurve(rocTable{:,'Labels'},rocTable{:,'have'},'have');
        plot(x1,y1);
        xlabel('False positive rate');
        ylabel('True positive rate');
        title(sprintf("%s - Overall BA Patients (AUC:%5.2f%%)",netNOnly, auc1*100));
        filename = sprintf("ROC_Curve_%s_Overall.png", netNOnly);
        destination = fullfile(writeDir, filename);
        saveas(gcf,destination);
        close(gcf);
    end       
end