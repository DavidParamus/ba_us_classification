imgDir =fullfile(pwd,'Dataset\simple\Train_image_Aug\');
imds = imageDatastore(imgDir, 'LabelSource', 'foldernames', 'IncludeSubfolders',true);

idx = size(imds.Files,1);
scores = cell(idx, 3);
fprintf('[INFO] Start calculateing PIQE score...\n');
for i = 1:idx
    if mod(i+1, 100) == 0
        fprintf('[INFO] calculateing PIQE Progress: %.4f %%\n', (i/idx)*100 );
    end
    [filepath,filename,ext] = fileparts(string(imds.Files(i)));
    im = imread(string(imds.Files(i)));
    score = piqe(im);
    scores{i,1} = filename + ext; 
    scores{i,2} = score;
end

piqes = cell2mat(scores(1:end,2));
fprintf('[INFO] Start separating validation and traing data...\n');
for i = 1:idx
    if mod(i+1, 10) == 0
        fprintf('[INFO] Separating Progress: %.4f %%\n', (i/idx)*100 );
    end
    if cell2mat(scores(i,2)) < quantile(piqes,0.1)
        scores{i,3} = 'Validation';
    else
        scores{i,3} = 'Train';
    end
end

scores = [{'Filename', 'Score','Type'} ; scores];
writecell(scores,'piqe_scores_simple.csv');
fprintf('[INFO] Calculation Completed!\n');