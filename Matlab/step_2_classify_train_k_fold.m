%% Clear
clc;
clear;
diary off;

%% Run Date
today = datetime('today');
%run_date = datestr(today, 'yyyy-mm-dd');
run_date = "2021-04-27";

%% Load Images
% image size
inputSize = [224 224 3];

trainDir =fullfile(pwd,'Dataset\square_20210426\Train_Image_Aug\');
imds = imageDatastore(trainDir, 'LabelSource', 'foldernames', 'IncludeSubfolders',true);
imds.ReadFcn = @(loc)imresize(imread(loc),inputSize(1:2));

% Determine the split up
total_split=countEachLabel(imds);

%{
%% Define and Train Network Architecture - 2 - ResNet 
netName= 'resnet18';
trainedNet=getResNet(total_split{:,1}, 'resnet18');
trainNetwokKFold(imds, trainedNet, netName, run_date);

netName= 'resnet50';
trainedNet=getResNet(total_split{:,1}, 'resnet50');
trainNetwokKFold(imds, trainedNet, netName, run_date);

netName= 'resnet101';
trainedNet=getResNet(total_split{:,1}, 'resnet101');
trainNetwokKFold(imds, trainedNet, netName, run_date);
%}

%% Define and Train Network Architecture - 3 - VGG
netName= 'vgg16';
trainedNet=getVGGNet(total_split{:,1}, 'vgg16');
trainNetwokKFold(imds, trainedNet, netName, run_date);

netName= 'vgg19';
trainedNet=getVGGNet(total_split{:,1}, 'vgg19');
trainNetwokKFold(imds, trainedNet, netName, run_date);


%% Define and Train Network Architecture - 4 -ShuffleNet
netName = 'shufflenet';
trainedNet = getShufflenet(total_split{:,1});
trainNetwokKFold(imds, trainedNet, netName, run_date);


%% Define and Train Network Architecture - 5 - GoogleNet
netName = 'googlenet';
trainedNet = getGoogLeNet(total_split{:,1});
trainNetwokKFold(imds, trainedNet, netName, run_date);


%% Define and Train Network Architecture - 6 - MoblieNet
netName = 'moblienetv2';
trainedNet = getGoogLeNet(total_split{:,1});
trainNetwokKFold(imds, trainedNet, netName, run_date);


%% Define and Train Network Architecture - 7 - DenseNet
netName = 'densenet201';
trainedNet = getDenseNet(total_split{:,1});
trainNetwokKFold(imds, trainedNet, netName, run_date);
fprintf("[INFO] Training all network completed!\n");

%% K-fold Validation
function trainNetwokKFold(imds, trainedNet, netName,  run_date)

fprintf("[INFO] Training network %s...\n", netName);

% image size
inputSize = [224 224 3];

% Determine the split up
total_split=countEachLabel(imds);

% Number of Images
num_images=length(imds.Labels);

result_dir = "Run_Result\";
if ~exist(fullfile(result_dir, run_date, netName, 'Train_Log'), 'dir')
    mkdir(fullfile(result_dir, run_date, netName, 'Train_Log'));
end

if ~exist(fullfile(result_dir, run_date, netName, 'networks'), 'dir')
    mkdir(fullfile(result_dir, run_date, netName, 'networks'));
end

row_datas = table;
num_folds=10;
% Loop for each fold
for fold_idx=1:num_folds
    
    fprintf('\n[INFO] Processing %d among %d folds \n',fold_idx,num_folds);
    filename_txt = sprintf("TrainingLog_%s_%s_%dfold_%d.txt", netName ,run_date, num_folds, fold_idx);
    diary(fullfile(result_dir, run_date, netName, 'Train_Log', filename_txt));
    diary on;
    
    % Test Indices for current fold
    test_idx=fold_idx:num_folds:num_images;

    % Test cases for current fold
    imdsTest = subset(imds,test_idx);
    
    % Train indices for current fold
    train_idx=setdiff(1:length(imds.Files),test_idx);
    
    % Train cases for current fold
    imdsTrain = subset(imds,train_idx);

    %rmsprop,sgdm,adam
    % Training Options, we choose a small mini-batch size due to limited images 
    if contains(netName, 'vgg')
        miniBatchSize = 20;
    else
        miniBatchSize = 25;
    end
    
    options = trainingOptions('sgdm', ...
        'InitialLearnRate', 0.001, ...
        'MaxEpochs',10, ...
        'MiniBatchSize',miniBatchSize, ...
        'Verbose',true,...
        'VerboseFrequency',50, ...
        'Shuffle','every-epoch', ...
        'Plots','training-progress', ...
        'LearnRateSchedule','piecewise', ...
        'LearnRateDropFactor',0.1, ...
        'LearnRateDropPeriod',2);
  
    % Resizing all training images to  for ResNet architecture
    auimds = augmentedImageDatastore(inputSize(1:2), imdsTrain, 'ColorPreprocessing', 'gray2rgb');
    
    % Training
    netTransfer = trainNetwork(auimds, trainedNet, options);
    
    % Save training log graph
    filename_gcf = sprintf("TrainingLogResult_%s_%d_among_%d_folds.png", netName, fold_idx, num_folds);
    training_gcf = findall(groot, 'Type', 'Figure');
    exportapp(training_gcf,fullfile(result_dir, run_date, netName, 'Train_log',filename_gcf));
    close(training_gcf);
    
    % Resizing all testing images to inputSize(1:2)(e.g. [224 224]) for CNN architecture   
    augtestimds = augmentedImageDatastore(inputSize(1:2),imdsTest);
    
    % Testing and their corresponding Labels and Posterior for each Case
    tic
    [predicted_labels(test_idx),posterior(test_idx,:)] = classify(netTransfer,augtestimds);
    runTime = toc;
    
    % Save the Independent CNN Architectures obtained for each Fold
    filenameNames = sprintf('%s_%d_among_%d_folds',netName,fold_idx,num_folds);
    networkNames = fullfile(result_dir, run_date, netName, "networks", filenameNames);
    save(networkNames,'netTransfer');    
    
    % Evaluation
    testLabels = imdsTest.Labels;
    tableNetName = sprintf("%s_%dfold_%d", netName,num_folds, fold_idx);
    row_data = evaluateNetwork(testLabels, predicted_labels(test_idx), tableNetName, runTime);
    row_datas = [row_datas; row_data];
    diary off;
end
% analyzeNetwork(netTransfer)
tableName = sprintf('PerformanceMetrics_%s_%dfold.csv', netName, num_folds);
writeDir = fullfile(result_dir, run_date, netName); 
writetable(row_datas, fullfile(writeDir, tableName));

end 

%% EvaluateNetwork
function row_data=evaluateNetwork(testLabels, predictedLabels, tableNetName, runTime)
    
confusionMat = confusionmat(testLabels, predictedLabels);
accuracy = ( confusionMat(1,1) + confusionMat(2,2) ) / sum(confusionMat(:));
precision = confusionMat(1,1) / (confusionMat(1,1) + confusionMat(2,1));
sensitivity = confusionMat(1,1) / (confusionMat(1,1) + confusionMat(1,2));
specificity = confusionMat(2,2) / (confusionMat(2,1) + confusionMat(2,2));
falsePositiveRate = confusionMat(2,1) / (confusionMat(2,1) + confusionMat(2,2));
falseNegativeRate = confusionMat(1,2) / (confusionMat(1,1) + confusionMat(1,2));
f1score = 2 / ( ( 1 / precision ) + (1 / sensitivity) );

fprintf("[INFO] Accuracy: %f%%\n", accuracy*100);
fprintf("[INFO] Precision: %f%%\n", precision*100);
fprintf("[INFO] Sensitivity: %f%%\n", sensitivity*100);
fprintf("[INFO] Specificity: %f%%\n", specificity*100);
fprintf("[INFO] False Positive Rate: %f%%\n", falsePositiveRate*100);
fprintf("[INFO] False Negative Rate: %f%%\n", falseNegativeRate*100);
fprintf("[INFO] F1-Measure: %f%%\n", f1score*100);
row_data = table({tableNetName}, accuracy, precision, sensitivity, specificity, falsePositiveRate, ...
                 falseNegativeRate, f1score, runTime, ... 
                 'VariableNames',{'Network Name','Accuracy', 'Precision', 'Sensitivity', 'Specificity',...
                 'False Positive Rate', 'False Negative Rate', 'F1-Score', 'Runtime'});

end