%% Clear console and workspace
clc;
clear;


%% Run Date
today = datetime('today');
run_date = datestr(today, 'yyyy-mm-dd');


%% Load Images
inputSize = [224 224 3];

testDir =fullfile(pwd,'Dataset\simple_20210720\Test_image\');
imdsRun = imageDatastore(testDir, 'LabelSource', 'foldernames', 'IncludeSubfolders',true);
imdsRun.ReadFcn = @(loc)imresize(imread(loc),inputSize(1:2));

writeDir = fullfile(pwd,'Run_Result', run_date, 'Run_Result');
if ~exist(writeDir, 'dir')
    mkdir(writeDir)
end

row_datas = table;

filename = sprintf("RunResultLog_%s.txt" ,run_date);
diary(fullfile(writeDir, filename));
diary on;

%% Load and Test Network Architecture - 1 - residualCIFARlgraph
netName_1 = 'simple_1_CIFARNet_20_standard.mat';
fprintf("[INFO] Loading network ""%s"" ...\n", netName_1);
load(fullfile('networks', netName_1), 'trainedNet');
row_data = testTrainNetwok(imdsRun, trainedNet, netName_1, run_date);
row_datas = [row_datas; row_data];


%% Load and Test Network Architecture - 2 - ResNet
netName_2 = 'simple_2_resnet101.mat';
fprintf("[INFO] Loading network ""%s"" ...\n", netName_2);
load(fullfile('networks',netName_2), 'trainedNet');
row_data = testTrainNetwok(imdsRun, trainedNet, netName_2, run_date);
row_datas = [row_datas; row_data];


%% Load and Test Network Architecture - 3 - VGGNet
netName_3 = 'simple_3_vgg16.mat';
fprintf("[INFO] Loading network ""%s"" ...\n", netName_3);
load(fullfile('networks',netName_3), 'trainedNet');
row_data = testTrainNetwok(imdsRun, trainedNet, netName_3, run_date);
row_datas = [row_datas; row_data];


%% Load and Test Network Architecture - 4 - Shufflenet
netName_4 = 'simple_4_shufflenet.mat';
fprintf("[INFO] Loading network ""%s"" ...\n", netName_4);
load(fullfile('networks',netName_4), 'trainedNet');
row_data = testTrainNetwok(imdsRun, trainedNet, netName_4, run_date);
row_datas = [row_datas; row_data];


%% Load and Test Network Architecture - 5 - GoogleNet
netName_5 = 'simple_5_googlenet.mat';
fprintf("[INFO] Loading network ""%s"" ...\n", netName_5);
load(fullfile('networks',netName_5), 'trainedNet');
row_data = testTrainNetwok(imdsRun, trainedNet, netName_5, run_date);
row_datas = [row_datas; row_data];


%% Load and Test Network Architecture - 6 - MoblieNet
netName_6 = 'simple_6_moblienetv2.mat';
fprintf("[INFO] Loading network ""%s"" ...\n", netName_6);
load(fullfile('networks',netName_6), 'trainedNet');
row_data = testTrainNetwok(imdsRun, trainedNet, netName_6, run_date);
row_datas = [row_datas; row_data];


%% Load and Test Network Architecture - 7 - DenseNet
netName_7 = 'simple_7_densenet201.mat';
fprintf("[INFO] Loading network ""%s"" ...\n", netName_7);
load(fullfile('networks',netName_7), 'trainedNet');
row_data = testTrainNetwok(imdsRun, trainedNet, netName_7, run_date);
row_datas = [row_datas; row_data];

diary off;
writetable(row_datas, fullfile(writeDir, 'DataSetMetrics.csv'));

%% TestTrainNetwok
function row_data=testTrainNetwok(imdsRun, trainedNet, netName, run_date)
% Evaluate Trained Network
writeDir = fullfile(pwd,'Run_Result', run_date, 'Run_Result');
if ~exist(writeDir, 'dir')
    mkdir(writeDir);
end

tic
[testPred,test_scores] = classify(trainedNet, imdsRun);
runTime = toc;
fprintf('[INFO] Overall test images executive time: %f sec\n', runTime);

% Display random sample pf nine test images together with predicted classes and the probabilities of those classes
%{
figure;

img_index = 1;
for i = 1:size(imdsRun.Files,1)
    if mod(i,25) == 0
        subplot(5,5, 25);
    else
        subplot(5,5, mod(i,25));
    end    
    
    im = imread(string(imdsRun.Files(i)));
    imshow(im);
    prob = num2str(100*max(test_scores(i,:)),3);
    predClass = char(testPred(i));
    
    title([predClass,', ',prob,'%, ']);
    %xlabel(predCorrect)
    
    if mod(i, 25) == 0
        filename = sprintf("RunResult_%s_%d.png", netName, img_index);
        if ~exist(fullfile(writeDir, netName), 'dir')
            mkdir(fullfile(writeDir, netName));
        end
        destination = fullfile(writeDir, netName, filename);
        saveas(gcf,destination);
        close(gcf);
        img_index = img_index + 1;
        figure;
    end
    
end

filename = sprintf("RunResult_%s_%d.png", netName, img_index);
destination = fullfile(writeDir, filename);
saveas(gcf,destination);
close(gcf);

predicated = countcats(testPred,1);

row_BA_possibility = predicated(1) / size(imdsRun.Files, 1);

row_data = table({netName}, row_BA_possibility, runTime, ... 
                 'VariableNames',{'Network Name', 'row_BA_possibility', 'Runtime'});
%}


end
