function lgraph=getResNet(classes,resnet_type)
if resnet_type == "resnet18"
    lgraph = resnet18('Weights','none');
    fcLayer = fullyConnectedLayer(size(classes,1), 'Name', 'FC_Neonatal_jaundice');
    lgraph = replaceLayer(lgraph,'fc1000',fcLayer);
    
    smLayer = softmaxLayer('Name','Softmax_Neonatal_jaundice');
    lgraph = replaceLayer(lgraph,'prob',smLayer);
    
    cLayer = lgraph.Layers(end);
    cLayer.Classes = string(classes);
    cLayer.Name = "Classify_Neonatal_jaundice";
    lgraph = replaceLayer(lgraph,'ClassificationLayer_predictions',cLayer);
elseif resnet_type ==  "resnet50"
    lgraph = resnet50('Weights','none');
    fcLayer = fullyConnectedLayer(size(classes,1), 'Name', 'FC_Neonatal_jaundice');
    lgraph = replaceLayer(lgraph,'fc1000',fcLayer);
    
    smLayer = softmaxLayer('Name','Softmax_Neonatal_jaundice');
    lgraph = replaceLayer(lgraph,'fc1000_softmax',smLayer);
    
    cLayer = lgraph.Layers(end);
    cLayer.Classes = string(classes);
    cLayer.Name = "Classify_Neonatal_jaundice";
    lgraph = replaceLayer(lgraph,'ClassificationLayer_fc1000',cLayer);
elseif resnet_type == "resnet101"
    lgraph = resnet101('Weights','none');
    fcLayer = fullyConnectedLayer(size(classes,1), 'Name', 'FC_Neonatal_jaundice');
    lgraph = replaceLayer(lgraph,'fc1000',fcLayer);
    
    smLayer = softmaxLayer('Name','Softmax_Neonatal_jaundice');
    lgraph = replaceLayer(lgraph,'prob',smLayer);
    
    cLayer = lgraph.Layers(end);
    cLayer.Classes = string(classes);
    cLayer.Name = "Classify_Neonatal_jaundice";
    lgraph = replaceLayer(lgraph,'ClassificationLayer_predictions',cLayer);
end
end
