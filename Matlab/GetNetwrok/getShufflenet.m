function lgraph=getShufflenet(classes)
net = shufflenet();
lgraph = layerGraph(net);

fcLayer = fullyConnectedLayer(size(classes,1), 'Name', 'FC_Neonatal_jaundice');
lgraph = replaceLayer(lgraph,'node_202',fcLayer);

smLayer = softmaxLayer('Name','Softmax_Neonatal_jaundice');
lgraph = replaceLayer(lgraph,'node_203',smLayer);

cLayer = lgraph.Layers(end);
cLayer.Classes = string(classes);
cLayer.Name = "Classify_Neonatal_jaundice";
lgraph = replaceLayer(lgraph,'ClassificationLayer_node_203',cLayer);
end