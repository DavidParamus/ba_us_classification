function lgraph=getGoogLeNet(classes)
lgraph = googlenet('Weights','none');

fcLayer = fullyConnectedLayer(size(classes,1), 'Name', 'FC_Neonatal_jaundice');
lgraph = replaceLayer(lgraph,'loss3-classifier',fcLayer);

smLayer = softmaxLayer('Name','Softmax_Neonatal_jaundice');
lgraph = replaceLayer(lgraph,'prob',smLayer);

cLayer = lgraph.Layers(end);
cLayer.Classes = string(classes);
cLayer.Name = "Classify_Neonatal_jaundice";
lgraph = replaceLayer(lgraph,'output',cLayer);

end