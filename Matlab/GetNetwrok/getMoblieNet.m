function lgraph=getMoblieNet(classes)
lgraph = mobilenetv2('Weights','none');

fcLayer = fullyConnectedLayer(size(classes,1), 'Name', 'FC_Neonatal_jaundice');
lgraph = replaceLayer(lgraph,'Logits',fcLayer);

smLayer = softmaxLayer('Name','Softmax_Neonatal_jaundice');
lgraph = replaceLayer(lgraph,'Logits_softmax',smLayer);

cLayer = lgraph.Layers(end);
cLayer.Classes = string(classes);
cLayer.Name = "Classify_Neonatal_jaundice";
lgraph = replaceLayer(lgraph,'ClassificationLayer_Logits',cLayer);

end 