 function lgraph=getVGGNet(classes, vgg_type)
if vgg_type == "vgg16"
    lgraph = vgg16('Weights','none');
    lgraph(39) = fullyConnectedLayer(size(classes,1), 'Name', 'fc_Neonatal_jaundice');
    lgraph(40) = softmaxLayer('Name','Softmax_Neonatal_jaundice');
    cLayer = lgraph(end);
    cLayer.Classes = string(classes);
    cLayer.Name = "Classify_Neonatal_jaundice";
    lgraph(end) = cLayer;
elseif vgg_type == "vgg19"
    lgraph = vgg19('Weights','none');
    lgraph(45) = fullyConnectedLayer(size(classes,1), 'Name', 'fc_Neonatal_jaundice');
    lgraph(46) = softmaxLayer('Name','Softmax_Neonatal_jaundice');
    cLayer = lgraph(end);
    cLayer.Classes = string(classes);
    cLayer.Name = "Classify_Neonatal_jaundice";
    lgraph(end) = cLayer;
end
end