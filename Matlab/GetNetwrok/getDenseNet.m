function lgraph=getDenseNet(classes)
lgraph = densenet201('Weights','none');

fcLayer = fullyConnectedLayer(size(classes,1), 'Name', 'FC_Neonatal_jaundice');
lgraph = replaceLayer(lgraph,'fc1000',fcLayer);

smLayer = softmaxLayer('Name','Softmax_Neonatal_jaundice');
lgraph = replaceLayer(lgraph,'fc1000_softmax',smLayer);

cLayer = lgraph.Layers(end);
cLayer.Classes = string(classes);
cLayer.Name = "Classify_Neonatal_jaundice";
lgraph = replaceLayer(lgraph,'ClassificationLayer_fc1000',cLayer);

end