%% Clear console and workspace
clc;
clear;
close all;

%% Run Date
today = datetime('today');
run_date = datestr(today, 'yyyy-mm-dd');


%% Load Images
inputSize = [224 224 3];

testDir =fullfile(pwd,'Dataset\simple_20210608\none');
%testDir =fullfile(pwd,'Dataset\simple_20210720\have');
%testDir =fullfile(pwd,'Dataset\simple_20210812\have');
imdsTest = imageDatastore(testDir, 'LabelSource', 'foldernames', 'IncludeSubfolders',true);
imdsTest.ReadFcn = @(loc)imresize(imread(loc),inputSize(1:2));

writeDir = fullfile(pwd,'Run_Result', run_date, ['Run_Result', '-20210608']);
%writeDir = fullfile(pwd,'Run_Result', run_date, ['Run_Result', '-20210720']);
%writeDir = fullfile(pwd,'Run_Result', run_date, ['Run_Result', '-20210812']);
if ~exist(writeDir, 'dir')
    mkdir(writeDir)
end

row_datas = table;

filename = sprintf("TestResultLog_%s.txt" ,run_date);
diary(fullfile(writeDir, filename));
diary on;

networks_path = fullfile('networks', 'networks_2021-04-08');

%% Load and Test Network Architecture - 1 - residualCIFARlgraph
%{
netName_1 = 'simple_1_CIFARNet_20_standard.mat';
fprintf("[INFO] Loading network ""%s"" ...\n", netName_1);
load(fullfile(networks_path, netName_1), 'trainedNet');
row_data = testTrainNetwok(imdsTest, trainedNet, netName_1, run_date, writeDir);
row_datas = [row_datas; row_data];
%}

%% Load and Test Network Architecture - 2 - ResNet
netName_2_1 = 'simple_2_resnet18.mat';
fprintf("[INFO] Loading network ""%s"" ...\n", netName_2_1);
load(fullfile(networks_path, netName_2_1), 'trainedNet');
row_data = testTrainNetwok(imdsTest, trainedNet, netName_2_1, run_date, writeDir);
row_datas = [row_datas; row_data];

netName_2_2 = 'simple_2_resnet50.mat';
fprintf("[INFO] Loading network ""%s"" ...\n", netName_2_2);
load(fullfile(networks_path, netName_2_2), 'trainedNet');
row_data = testTrainNetwok(imdsTest, trainedNet, netName_2_2, run_date, writeDir);
row_datas = [row_datas; row_data];

netName_2_3 = 'simple_2_resnet101.mat';
fprintf("[INFO] Loading network ""%s"" ...\n", netName_2_3);
load(fullfile(networks_path, netName_2_3), 'trainedNet');
row_data = testTrainNetwok(imdsTest, trainedNet, netName_2_3, run_date, writeDir);
row_datas = [row_datas; row_data];



%% Load and Test Network Architecture - 3 - VGGNet
netName_3_1 = 'simple_3_vgg16.mat';
fprintf("[INFO] Loading network ""%s"" ...\n", netName_3_1);
load(fullfile(networks_path,netName_3_1), 'trainedNet');
row_data = testTrainNetwok(imdsTest, trainedNet, netName_3_1, run_date, writeDir);
row_datas = [row_datas; row_data];

netName_3_2 = 'simple_3_vgg19.mat';
fprintf("[INFO] Loading network ""%s"" ...\n", netName_3_2);
load(fullfile(networks_path,netName_3_2), 'trainedNet');
row_data = testTrainNetwok(imdsTest, trainedNet, netName_3_2, run_date, writeDir);
row_datas = [row_datas; row_data];


%% Load and Test Network Architecture - 4 - Shufflenet
netName_4 = 'simple_4_shufflenet.mat';
fprintf("[INFO] Loading network ""%s"" ...\n", netName_4);
load(fullfile(networks_path, netName_4), 'trainedNet');
row_data = testTrainNetwok(imdsTest, trainedNet, netName_4, run_date, writeDir);
row_datas = [row_datas; row_data];


%% Load and Test Network Architecture - 5 - GoogleNet
netName_5 = 'simple_5_googlenet.mat';
fprintf("[INFO] Loading network ""%s"" ...\n", netName_5);
load(fullfile(networks_path, netName_5), 'trainedNet');
row_data = testTrainNetwok(imdsTest, trainedNet, netName_5, run_date, writeDir);
row_datas = [row_datas; row_data];


%% Load and Test Network Architecture - 6 - MoblieNet
netName_6 = 'simple_6_moblienetv2.mat';
fprintf("[INFO] Loading network ""%s"" ...\n", netName_6);
load(fullfile(networks_path, netName_6), 'trainedNet');
row_data = testTrainNetwok(imdsTest, trainedNet, netName_6, run_date, writeDir);
row_datas = [row_datas; row_data];


%% Load and Test Network Architecture - 7 - DenseNet
netName_7 = 'simple_7_densenet201.mat';
fprintf("[INFO] Loading network ""%s"" ...\n", netName_7);
load(fullfile(networks_path, netName_7), 'trainedNet');
row_data = testTrainNetwok(imdsTest, trainedNet, netName_7, run_date, writeDir);
row_datas = [row_datas; row_data];

row_data = table({'Average'}, mean(row_datas(:,'BA_possibility').Variables),  mean(row_datas(:,'Runtime').Variables), ... 
                 'VariableNames',{'Network Name', 'row_BA_possibility', 'Runtime'});
row_datas = [row_datas; row_data];

diary off;
writetable(row_datas, fullfile(writeDir, 'DataSetMetrics.xlsx'));


%% TestTrainNetwok
function row_data=testTrainNetwok(imdsTest, trainedNet, netName, run_date, writeDir)
% Evaluate Trained Network

tic
[testPred,test_scores] = classify(trainedNet, imdsTest);
runTime = toc;
fprintf('[INFO] Overall test images executive time: %f sec\n', runTime);

% Display random sample pf nine test images together with predicted classes and the probabilities of those classes
figure;

img_index = 1;
for i = 1:size(imdsTest.Files,1)
    if mod(i,25) == 0
        subplot(5,5, 25);
    else
        subplot(5,5, mod(i,25));
    end    
    
    im = imread(string(imdsTest.Files(i)));
    imshow(im);
    prob = num2str(100*max(test_scores(i,:)),3);
    predClass = char(testPred(i));
    
    title([predClass,', ',prob,'%, ']);
    %xlabel(predCorrect)
    
    if mod(i, 25) == 0
        filename = sprintf("TestResult_%s_%d.png", netName, img_index);
        if ~exist(fullfile(writeDir, netName), 'dir')
            mkdir(fullfile(writeDir, netName));
        end
        destination = fullfile(writeDir, netName, filename);
        saveas(gcf,destination);
        close(gcf);
        img_index = img_index + 1;
        figure;
    end
    
end

filename = sprintf("TestResult_%s_%d.png", netName, img_index);
destination = fullfile(writeDir, filename);
saveas(gcf,destination);
close(gcf);

predicated = countcats(testPred,1);

row_BA_possibility = predicated(1) / size(imdsTest.Files, 1);

row_data = table({netName}, row_BA_possibility, runTime, ... 
                 'VariableNames',{'Network Name', 'BA_possibility', 'Runtime'});
end