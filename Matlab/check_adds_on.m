%% Clear console and workspace
clc;
clear;
fprintf("[INFO] Benigning chekcing network add-ons...\n");


%% Check VGG net
fprintf("[INFO] Chcecking VGGNet...\n");
vgg16();
vgg19();
fprintf("[INFO] All VGGNet add-on installed.\n\n");

%% Check ResNet
fprintf("[INFO] Chcecking ResNet...\n");
resnet18();
resnet50();
resnet101();
fprintf("[INFO] All ResNet add-on installed.\n\n");

%% check DenseNet201
fprintf("[INFO] Chcecking DenseNet...\n");
densenet201();
fprintf("[INFO] All DenseNet add-on installed.\n\n");

%% Check MobileNet
fprintf("[INFO] Chcecking MobileNet...\n");
mobilenetv2();
fprintf("[INFO] All MobileNet add-on installed.\n\n");

%% shufflenet
fprintf("[INFO] Chcecking ShuffleNet...\n");
shufflenet();
fprintf("[INFO] All ShuffleNet add-on installed.\n\n");

%% googlenet
fprintf("[INFO] Chcecking GoogleNet...\n");
googlenet();
fprintf("[INFO] All GoogleNet add-on installed.\n");
