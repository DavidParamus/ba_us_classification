%% Clear console and workspace
clc;
clear;
diary off;


%% Run Date
today = datetime('today');
run_date = datestr(today, 'yyyy-mm-dd');
%run_date = '2021-04-19';
run_date = [run_date, '_patient'];

overall_results = table;
%{
%% Load and Test Network Architecture - 1 - residualCIFARlgraph
netName_1 = 'simple_1_CIFARNet_20_standard.mat';
fprintf("[INFO] Loading network ""%s"" ...\n", netName_1);
load(fullfile('networks', netName_1), 'trainedNet');
row_data = testTrainNetwok(trainedNet, netName_1, run_date);

overall_results = [overall_results; row_data];
%}

%% Load and Test Network Architecture - 2 - ResNet
netName_2 = 'resnet18_3_among_10_folds.mat';
fprintf("[INFO] Loading network ""%s"" ...\n", netName_2);
load(fullfile('networks',netName_2), 'netTransfer');
row_data = testTrainNetwok(netTransfer, netName_2, run_date);
overall_results = [overall_results; row_data];

netName_2 = 'resnet50_8_among_10_folds.mat';
fprintf("[INFO] Loading network ""%s"" ...\n", netName_2);
load(fullfile('networks',netName_2), 'netTransfer');
row_data = testTrainNetwok(netTransfer, netName_2, run_date);
overall_results = [overall_results; row_data];

netName_2 = 'resnet101_5_among_10_folds.mat';
fprintf("[INFO] Loading network ""%s"" ...\n", netName_2);
load(fullfile('networks',netName_2), 'netTransfer');
row_data = testTrainNetwok(netTransfer, netName_2, run_date);
overall_results = [overall_results; row_data];


%% Load and Test Network Architecture - 3 - VGGNet
netName_3 = 'vgg16_5_among_10_folds.mat';
fprintf("[INFO] Loading network ""%s"" ...\n", netName_3);
load(fullfile('networks',netName_3), 'netTransfer');
row_data = testTrainNetwok(netTransfer, netName_3, run_date);
overall_results = [overall_results; row_data];

netName_3 = 'vgg19_4_among_10_folds.mat';
fprintf("[INFO] Loading network ""%s"" ...\n", netName_3);
load(fullfile('networks',netName_3), 'netTransfer');
row_data = testTrainNetwok(netTransfer, netName_3, run_date);
overall_results = [overall_results; row_data];

%% Load and Test Network Architecture - 4 - Shufflenet
netName_4 = 'shufflenet_1_among_10_folds.mat';
fprintf("[INFO] Loading network ""%s"" ...\n", netName_4);
load(fullfile('networks',netName_4), 'netTransfer');
row_data = testTrainNetwok(netTransfer, netName_4, run_date);
overall_results = [overall_results; row_data];


%% Load and Test Network Architecture - 5 - GoogleNet
netName_5 = 'googlenet_9_among_10_folds.mat';
fprintf("[INFO] Loading network ""%s"" ...\n", netName_5);
load(fullfile('networks',netName_5), 'netTransfer');
row_data = testTrainNetwok(netTransfer, netName_5, run_date);
overall_results = [overall_results; row_data];


%% Load and Test Network Architecture - 6 - MoblieNet
netName_6 = 'moblienetv2_9_among_10_folds.mat';
fprintf("[INFO] Loading network ""%s"" ...\n", netName_6);
load(fullfile('networks',netName_6), 'netTransfer');
row_data = testTrainNetwok(netTransfer, netName_6, run_date);
overall_results = [overall_results; row_data];


%% Load and Test Network Architecture - 7 - DenseNet
netName_7 = 'densenet201_9_among_10_folds.mat';
fprintf("[INFO] Loading network ""%s"" ...\n", netName_7);
load(fullfile('networks',netName_7), 'netTransfer');
row_data = testTrainNetwok(netTransfer, netName_7, run_date);
overall_results = [overall_results; row_data];


writeDir = fullfile(pwd,'Run_Result', run_date, 'Test_Result');
writetable(overall_results, fullfile(writeDir, 'Overall_patient_DataSetMetrics.csv'));


%% TestTrainNetwok
function row_data=testTrainNetwok(trainedNet, netName, run_date)

% Load Images
inputSize = [224 224 3];

% Write log
writeDir = fullfile(pwd,'Run_Result', run_date, 'Test_Result', netName);
if ~exist(writeDir, 'dir')
    mkdir(writeDir)
end
filename = sprintf("TestResultLog_%s.txt" ,run_date);
diary(fullfile(writeDir, filename));
diary on;

testDir =fullfile(pwd,'Dataset/square_20210426/Test_image_patients/');
patients = removeUnnecessaryData(dir(fullfile(testDir)));
row_datas = table;
for i = 1:size(patients,1)
    % Load patient test data
    patient = patients(i).name;
    fprintf("    [INFO] Testing patient: %s (%d/%d)\n", patient, i,size(patients,1));
    testDir_patient =fullfile(testDir, patient);
    imdsTest = imageDatastore(testDir_patient, 'LabelSource', 'foldernames', 'IncludeSubfolders',true);
    imdsTest.ReadFcn = @(loc)imresize(imread(loc),inputSize(1:2));
    
    writeDir_patient = fullfile(pwd,'Run_Result', run_date, 'Test_Result', netName, patient);
    if ~exist(writeDir_patient, 'dir')
        mkdir(writeDir_patient)
    end
    
    % Evaluate Trained Network
    testLabels = imdsTest.Labels;
    patient_type = testLabels(1);
    tic
    [testPred,test_scores] = classify(trainedNet, imdsTest, 'MiniBatchSize', 50);
    runTime = toc;
    fprintf("    [INFO] Overall test images executive time: %f sec\n", runTime);
    testError = mean(testPred ~= testLabels);
    fprintf("    [INFO] Test error: %f %%\n", testError*100);
    accuracy = sum(testPred == testLabels)/numel(testLabels);
    fprintf("    [INFO] Accuracy: %f %%\n", accuracy*100);
    fprintf("\n"); 

    figure('Units','normalized','Position',[0.2 0.2 0.4 0.4]);
    cm = confusionchart(testLabels,testPred);
    cm.Title = 'Confusion Matrix for Test Data';
    %cm.ColumnSummary = 'column-normalized';
    %cm.RowSummary = 'row-normalized';

    filename = sprintf("ConfusionMatrix_Test_%s.png", netName);
    destination = fullfile(writeDir_patient, filename);
    saveas(gcf,destination)
    close(gcf)

    % Display random sample pf nine test images together with predicted classes and the probabilities of those classes
    figure;

    img_index = 1;
    for j = 1:size(imdsTest.Files,1)
        if mod(j,25) == 0
            subplot(5,5, 25);
        else
            subplot(5,5, mod(j,25));
        end    

        im = imread(string(imdsTest.Files(j)));
        imshow(im);
        prob = num2str(100*max(test_scores(j,:)),3);
        predClass = char(testPred(j));

        %fprintf("[DEBUG] %s vs. %s : %d\n", testPred(i), testLabels(i), testPred(i) == testLabels(i) );
        if testPred(j) == testLabels(j)
            predCorrect='O';
        else
            predCorrect='X';
        end

        title([predClass,', ',prob,'%, ', predCorrect])
        %xlabel(predCorrect)

        if mod(j, 25) == 0
            filename = sprintf("TestResult_%s_%d.png", netName, img_index);
            destination = fullfile(writeDir_patient, filename);
            saveas(gcf,destination);
            close(gcf);
            img_index = img_index + 1;
            figure;
        end

    end
    filename = sprintf("TestResult_%s_%d.png", netName, img_index);
    destination = fullfile(writeDir_patient, filename);
    saveas(gcf,destination);
    close(gcf);
    
    predicated = countcats(testPred,1);
    
    row_BA_possibility = predicated(1) / size(imdsTest.Files, 1);
    
    row_data = table({netName}, {patient}, patient_type, size(imdsTest.Files, 1), predicated(1), ...
                      predicated(2), accuracy, row_BA_possibility, testError, runTime, ... 
                     'VariableNames',{'Network Name','Patient', 'Type', 'Amount of Images','Predicated (have)',...
                                      'Predicated (none)','Accuracy', 'BA Possibility', 'Test Error', 'Runtime (s)'});
    
    row_datas = [row_datas; row_data];
end

row_imgCount = row_datas{:, 4};
row_pred_1 = row_datas{:, 5};
row_pred_2 = row_datas{:, 6};
row_accuracy = row_datas{:, 7};
row_BA_possibility = row_datas{:, 8};
row_testError = row_datas{:, 9};
row_runTime = row_datas{:, 10};
row_data = table({netName}, "Overall", "Mean", sum(row_imgCount), sum(row_pred_1), ...
                 sum(row_pred_2), mean(row_accuracy), mean(row_BA_possibility), mean(row_testError), mean(row_runTime), ... 
                 'VariableNames',{'Network Name','Patient', 'Type', 'Amount of Images', 'Predicated (have)', ...
                                  'Predicated (none)', 'Accuracy', 'BA Possibility', 'Test Error', 'Runtime (s)'});
row_datas = [row_datas; row_data];
tablename = sprintf('%s_DataSetMetrics.csv', netName);
writetable(row_datas, fullfile(writeDir, tablename));
diary off;
end

function patients=removeUnnecessaryData(patients)

index = 1;
for i = 1:size(patients, 1)
    switch patients(index).name
        case '.'
            patients(index) = [];
            index = 1;
        case '..'
            patients(index) = [];
            index = 1;
        case 'desktop.ini'
            patients(index) = [];
            index = 1;
        otherwise
            index = index + 1;
    end 
end 

end 