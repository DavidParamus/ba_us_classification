import cv2
import os

database = "../Matlab/Database/complex"
threshold = 10

def calibrate_mask():
    mask = cv2.imread("mask.jpg", cv2.IMREAD_GRAYSCALE)
    #cv2.imshow("mask", mask)
    #cv2.waitKey(0)
    img_h, img_w = mask.shape[:2]
    print("{}, {}".format(img_h, img_w))
    for y in range(img_h):
        for x in range(img_w):
            if mask[y][x] > threshold:
                mask[y][x] = 255
            else:
                mask[y][x] = 0
    cv2.imwrite("mask.jpg", mask)

def calibrate_mask_complex():
    mask = cv2.imread("mask_2.png", cv2.IMREAD_GRAYSCALE)
    # cv2.imshow("mask", mask)
    # cv2.waitKey(0)
    img_h, img_w = mask.shape[:2]
    print("{}, {}".format(img_h, img_w))
    for y in range(img_h):
        for x in range(img_w):
            if mask[y][x] > threshold:
                mask[y][x] = 255
            else:
                mask[y][x] = 0
    cv2.imshow("mask", mask)
    cv2.waitKey(0)
    cv2.imwrite("mask_2.png", mask)

def img_rename_and_crop(image_type,target_path): 
    print("[INFO] Image rename and cropping...")
    corp_coor = (140, 70)  # 裁切起始位置
    crop_size = (650, 520) # 裁切大小
    img_folders = os.listdir(target_path)
    img_id = 0
    for img_folder in img_folders:
        print("[INFO] Progress folder: {}".format(img_folder))
        f_names = os.listdir(os.path.join(target_path, img_folder))
        f_names.sort()
        for fname in f_names:
            img = cv2.imread(os.path.join(target_path, img_folder, fname))
            # rename image
            folder_split = img_folder.replace("\n","").split(' ')
            new_fname = "%05d_%s_%s_%s" % (img_id, image_type, folder_split[0], fname)
            # Crop ROI
            img = img[ corp_coor[1]:corp_coor[1]+crop_size[1],
                       corp_coor[0]:corp_coor[0]+crop_size[0] ]
            #cv2.imshow(new_fname, img)
            #cv2.waitKey(0)
            cv2.imwrite(os.path.join(database, image_type, new_fname),img)
            img_id = img_id+1
    print("[INFO] Image rename and crop completed!")

if __name__=="__main__":
    # calibrate_mask()
    calibrate_mask_complex()
    # have_path = '../Dataset/Have'
    # none_path = '../Dataset/None'
    #img_rename_and_crop('have', have_path)
    #img_rename_and_crop('none', none_path)