import shutil
import os
import cv2

#pwd = "D:/DST/作業專區/資工系/108 碩士計畫\利用肝膽超音波評估新生兒及嬰兒之黃疸病狀"
database = "../Matlab/Database/simple_20210830"

folder1_crop_coor = (150, 75)  # 裁切起始位置
folder1_crop_size = (615, 445) # 裁切大小

folder1_1_crop_coor = (125, 75)  # 裁切起始位置
folder1_1_crop_size = (675, 445) # 裁切大小

folder2_crop_coor = (34, 50)  # 裁切起始位置
folder2_crop_size = (579, 365) # 裁切大小

folder2_1_crop_coor = (27, 50)  # 裁切起始位置
folder2_1_crop_size = (586, 350) # 裁切大小

folder3_crop_coor = (20, 30)  # 裁切起始位置
folder3_crop_size = (595, 420) # 裁切大小

folder9_crop_coor = (85, 100)  # Starting point
folder9_crop_size = (852, 595) # Crop size

def img_rename_and_crop(image_type, target_path, maskType, rewrite=False):
    print("[INFO] Rename and Crop \"{}\" \"{}\" image: ".format(image_type, maskType))
    fp = None
    datatables = None
    img_id = 1 
    if rewrite is True:
        print("[WARNING] Rewriting data...")
        if os.path.exists(os.path.join(database, image_type)):
            shutil.rmtree(os.path.join(database, image_type))
        os.makedirs(os.path.join(database, image_type))
        fp = open(os.path.join(database, "Baby_{}_tb.csv".format(image_type)), "w")
        fp.write("id,type,origin filename,new filename,TB,DB,isFeed,selected,remark\n")
    else: 
        print("[INFO] Appending new data...")
        fp = open(os.path.join(database, "Baby_{}_tb.csv".format(image_type)), "r")
        datatables = fp.readlines()
        fp.close()
        img_id = int(datatables[-1].split(",")[0]) + 1 
        print("[DEBUG] Final ID: %d" % img_id)
        fp = open(os.path.join(database, "Baby_{}_tb.csv".format(image_type)), "a")
    
    # print("[DEBUG] Now path: {}".format(os.getcwd()))
    # print("[DEBUG] Target path: {}".format(target_path))
    # print("[DEBUG] Target absPath: {}".format(os.path.abspath(target_path)))
    img_folders = os.listdir(target_path)

    if rewrite != True:
        temp_folders = []
        for img_folder in img_folders:
            found = False
            for datatable in datatables:
                data = datatable.split(",")
                origin_name = data[2]
                if origin_name[:origin_name.find(os.path.sep)] == img_folder:
                    found = True
                    break
            if found == False:
                temp_folders.append(img_folder)
        img_folders = temp_folders
    
    if maskType=="type_1":
        mask = cv2.imread("./simple_2/mask_type1.jpg", cv2.IMREAD_GRAYSCALE)
        corp_coor = folder1_crop_coor
        corp_size = folder1_crop_size
    elif maskType == "type_1_1":
        mask = cv2.imread("./simple_2/mask_type1_1.jpg", cv2.IMREAD_GRAYSCALE)
        corp_coor = folder1_1_crop_coor
        corp_size = folder1_1_crop_size
    elif maskType == "type_2":
        mask = cv2.imread("./simple_2/mask_type2.jpg", cv2.IMREAD_GRAYSCALE)        
        corp_coor = folder2_crop_coor
        corp_size = folder2_crop_size
    elif maskType == "type_2_1":
        mask = cv2.imread("./simple_2/mask_type2_1.jpg", cv2.IMREAD_GRAYSCALE)        
        corp_coor = folder2_1_crop_coor
        corp_size = folder2_1_crop_size
    elif maskType == "type_3":
        mask = cv2.imread("./simple_2/mask_type3.jpg", cv2.IMREAD_GRAYSCALE)        
        corp_coor = folder3_crop_coor
        corp_size = folder3_crop_size
    elif maskType == "type_9":
        mask = cv2.imread("./simple_2/mask_type9.jpg", cv2.IMREAD_GRAYSCALE)        
        corp_coor = folder9_crop_coor
        corp_size = folder9_crop_size
    else:
        print("[ERROR] maskType is not specified.")
        exit(0) 
    

    for img_folder in img_folders:
        print("[INFO] Progress folder: {}".format(img_folder))
        feed_folders = os.listdir(os.path.join(target_path, img_folder))
        feed_folders.sort()
       
        # print("[DEBUG] Is \'{}\' is a folder? {}".format(os.path.join(target_path, img_folder,feed_folders[0]), os.path.isdir(os.path.join(target_path, img_folder,feed_folders[0]))))
        if os.path.isdir(os.path.join(target_path, img_folder,feed_folders[0])):
            for feed_folder in feed_folders:
                if 'feeding' in feed_folder:
                    isFeeding = 1
                else:
                    isFeeding = 0
                
                f_names = os.listdir(os.path.join(target_path, img_folder, feed_folder))
                f_names.sort()
                for fname in f_names:
                    # print("[DEBUG] Target image: %s" % (fname))
                    img = cv2.imread(os.path.join(target_path, img_folder, feed_folder, fname))
                    # rename image
                    patient_id = img_folder
                    new_fname = "%05d_%s_%s_%s_%s" % (img_id, image_type, patient_id, feed_folder, fname)
                        
                    # Crop ROI
                    img = img[ corp_coor[1]:corp_coor[1]+corp_size[1],
                            corp_coor[0]:corp_coor[0]+corp_size[0] ]
       
                    # Replace ROI text to black
                    img_h, img_w = img.shape[:2]
                    for y in range(img_h):
                        for x in range(img_w):
                            if mask[y][x] <= 10:
                                img[y][x] = [0, 0, 0]

                    # Write Image
                    if not os.path.exists(os.path.join(database, image_type)):
                        os.path.join(database, image_type)
                    cv2.imwrite(os.path.join(database, image_type, new_fname),img)
            
                    # Write table
                    fp.write("%d,%s,%s,%s,%d,%d,%d,%d,\n" % (img_id, image_type, os.path.join(img_folder, fname), new_fname, 0, 0, isFeeding, 0))
                    img_id += 1
        else:
            f_names = os.listdir(os.path.join(target_path, img_folder))
            f_names.sort()
            for fname in f_names:
                # print("[DEBUG] Target image: %s" % (fname))
                img = cv2.imread(os.path.join(target_path, img_folder, fname))
                
                # rename image
                folder_split = img_folder.replace("\n","").split(' ')
                new_fname = "%05d_%s_%s_%s" % (img_id, image_type, folder_split[0], fname)
                
                # Crop ROI
                img = img[ corp_coor[1]:corp_coor[1]+corp_size[1],
                        corp_coor[0]:corp_coor[0]+corp_size[0] ]
                
                # Replace ROI text to black
                img_h, img_w = img.shape[:2]
                for y in range(img_h):
                    for x in range(img_w):
                        if mask[y][x] <= 0:
                            img[y][x] = [0, 0, 0]
                #write_img_dir = os.path.join(database, image_type)
                write_img_dir = os.path.join(database, image_type, img_folder)
                if not os.path.exists(write_img_dir):
                    os.makedirs(write_img_dir)
                cv2.imwrite(os.path.join(write_img_dir, new_fname),img)

                if len(folder_split) > 5:
                    fp.write("{},{},{},{},{},{},0,0,{}\n".format(img_id, image_type, os.path.join(img_folder, fname), new_fname, folder_split[-3], folder_split[-1], folder_split[-5]))
                elif len(folder_split) > 2:
                    fp.write("{},{},{},{},{},{},0,0,\n".format(img_id, image_type, os.path.join(img_folder, fname), new_fname, folder_split[-3], folder_split[-1]))
                else:
                    fp.write("{},{},{},{},0,0,0,0,\n".format(img_id, image_type, os.path.join(img_folder, fname), new_fname))
                img_id += 1
    fp.close()
    print("[INFO] Image rename and crop completed!")


def main():
    # 2020-04-09
    """
    print("[INFO] Starting preprocessing date \"2020-04-09\" 's dataset....")   
    have_path = '../Dataset/type1/2020-04-09/Have/'
    img_rename_and_crop('have', have_path, maskType="type_1", rewrite=True)

    none_path = '../Dataset/type1/2020-04-09/None/'
    img_rename_and_crop('none', none_path, maskType="type_1", rewrite=True)

    # 2020-11-11
    print("[INFO] Starting preprocessing date \"2020-11-11\" 's dataset....")   
    have_path = '../Dataset/type2/2020-11-11/have/'
    img_rename_and_crop('have', have_path, maskType="type_2")
    
    have_path = '../Dataset/type2/2020-11-11/none/'
    img_rename_and_crop('none', have_path, maskType="type_2")

    have_path = '../Dataset/type1/2020-11-11/have/'
    img_rename_and_crop('have', have_path, maskType="type_1")

    none_path = '../Dataset/type1/2020-11-11/none/'
    img_rename_and_crop('none', none_path, maskType="type_1")
    
    # 2021-03-10
    print("[INFO] Starting preprocessing date \"2021-03-10\" 's dataset....")   
    have_path = '../Dataset/type2/2021-03-10/have/'
    img_rename_and_crop('have', have_path, maskType="type_2")
    
    none_path = '../Dataset/type1/2021-03-10/none/'
    img_rename_and_crop('none', none_path, maskType="type_1")

    
    # 2021-03-30
    print("[INFO] Starting preprocessing date \"2021-03-30\" 's dataset....")   
    none_path = '../Dataset/type1/2021-03-30/none/'
    img_rename_and_crop('none', none_path, maskType="type_1")
    
    none_path = '../Dataset/type1_1/2021-03-30/none/'
    img_rename_and_crop('none', none_path, maskType="type_1_1")

    none_path = '../Dataset/type2/2021-03-30/none/'
    img_rename_and_crop('none', none_path, maskType="type_2")
    
    none_path = '../Dataset/type2_2/2021-03-30/none/'
    img_rename_and_crop('none', none_path, maskType="type_2_1")

    have_path = '../Dataset/type3/2021-03-30/have/'
    img_rename_and_crop('have', have_path, maskType="type_3")
    
    # 2021-06-08
    none_path = '../Dataset/type1/2021-06-08/none/'
    img_rename_and_crop('none', have_path, maskType="type_9", rewrite=True)
    

    # 2021-07-20
    have_path = '../Dataset/type9/2021-07-20/have/' # suspicions as the BA patient
    img_rename_and_crop('have', have_path, maskType="type_9", rewrite=True)
    
    
    # 2021-08-12
    have_path = '../Dataset/type9/2021-08-12/have/' # BA patient
    img_rename_and_crop('have', have_path, maskType="type_9", rewrite=True)
    

    # 2021-08-30
    none_path = '../Dataset/type9/2021-08-30/none/' # none patient
    img_rename_and_crop('none', none_path, maskType="type_9", rewrite=True)
    """
    
    # 2021-09-03
    unsigned_path = '../Dataset/type9/2021-09-03/unsigned/' # suspicions as the BA patient
    img_rename_and_crop('have', unsigned_path, maskType="type_9", rewrite=True)
    

if __name__ == '__main__':
    main()
    