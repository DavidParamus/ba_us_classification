import os
import cv2

def reFitter(target_path, write_dir, maskType):
    fnames = os.listdir(target_path)
    fnames.remove('desktop.ini')
    
    if maskType=="type_1":
        mask = cv2.imread("./simple_2/mask_type1.jpg", cv2.IMREAD_GRAYSCALE)
    elif maskType == "type_1_1":
        mask = cv2.imread("./simple_2/mask_type1_1.jpg", cv2.IMREAD_GRAYSCALE)
    elif maskType == "type_2":
        mask = cv2.imread("./simple_2/mask_type2.jpg", cv2.IMREAD_GRAYSCALE)        
    elif maskType == "type_2_1":
        mask = cv2.imread("./simple_2/mask_type2_1.jpg", cv2.IMREAD_GRAYSCALE)        
    elif maskType == "type_3":
        mask = cv2.imread("./simple_2/mask_type3.jpg", cv2.IMREAD_GRAYSCALE)        
    else:
        print("[ERROR] maskType is not specified.")
        exit(0) 
    # cv2.imshow("mask", mask)
    # cv2.waitKey(0) 

    for fname in fnames:
        print("[INFO] Progress file: {}".format(fname))
        img = cv2.imread(os.path.join(target_path, fname))
        # cv2.imshow(fname, img)
        # cv2.waitKey(0) 
        img_h, img_w = img.shape[:2]
        for y in range(img_h):
            for x in range(img_w):
                if mask[y][x] <= 10:
                    img[y][x] = [0, 0, 0]
        
        print("[DEBUG] Write path : {}".format(os.path.join(write_dir, fname)))
        cv2.imwrite(os.path.join(write_dir, fname),img)

if __name__ == "__main__":
    reFitter("./simple_2/Refiltter/", "./simple_2/RefitterResult/", maskType = "type_2_1")