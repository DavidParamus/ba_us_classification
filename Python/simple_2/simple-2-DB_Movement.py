import os


def mark_selected(target_table, selected_index):
    print("[INFO] Marking selected data")
    print("[INFO] Reading data table : {}".format(target_table))
    fp = open(target_table, "r")
    row_datas = fp.readlines()
    fp.close()
    title_row = row_datas[0]
    row_datas = row_datas[1:]
    for index, row_data in enumerate(row_datas):
        try:
            data = row_data.split(",")
            data_index = int(data[0])
            for selected in selected_index:
                if data_index == selected:
                    data[-2] = '1'
                    new_row = ",".join(data)
                    #print("[DEBUG] New_row : {}".format(new_row))
                    row_datas[index] = new_row
                    break
                else:
                    data[-2] = '0'
                    new_row = ",".join(data)
                    #print("[DEBUG] New_row : {}".format(new_row))
                    row_datas[index] = new_row
        except:
            print("[ERROR] row data is empyt : {}".format(row_data))
    row_datas.insert(0, title_row)
    # check_table(row_datas)
    print("[INFO] Rewriting data table : {}".format(target_table))
    fp = open(target_table, "w")
    fp.writelines(row_datas)
    fp.close()


def check_table(row_datas):
    print("[DEBUG] Check row_datas : ")
    for row_data in row_datas:
        print(row_data.replace("\n", ""))


def mark_data():

    print("[INFO] Remove 'BA' none B-mode ultrasound image...")
    target_tb = os.path.join(r"../Matlab/Database/simple/", "Baby_have_tb.csv")
    fp = open(target_tb, "r")
    datatables = fp.readlines()
    fp.close()
    selected_have = list(range(1, len(datatables)))
    print("[DEBUG] datatables len: {} ".format(len(datatables)))
    print("[DEBUG] last target: {}".format(selected_have[-1]))
    remove_target = [12, 13, 25, 28, 41, 46, 52, 56,
                     58, 62, 78, 79, 82, 105, 111, 123, 124, 127,
                     199, 200, 202, 207, 208, 209, 226, 227, 269, 
                     309, 318 ,321, 323, 344, 346, 385]
    for target in remove_target:
        selected_have.remove(target)
    mark_selected(target_tb, selected_have)



    print("[INFO] Remove 'non-BA' none B-mode ultrasound image...")
    target_tb = os.path.join(r"../Matlab/Database/simple/", "Baby_none_tb.csv")
    fp = open(target_tb, "r")
    datatables = fp.readlines()
    fp.close()
    selected_none = list(range(1, len(datatables)))
    print("[DEBUG] datatables len: {} ".format(len(datatables)))
    print("[DEBUG] last target: {}".format(selected_none[-1]))
    remove_target = [103, 357, 360, 676, 677,
                     1125, 1126, 1127, 1136, 1150,
                     1220, 1222, 1224, 1367, 1368]
    for target in remove_target:
        selected_none.remove(target)
    mark_selected(target_tb, selected_none)


if __name__ == "__main__":
    mark_data()
