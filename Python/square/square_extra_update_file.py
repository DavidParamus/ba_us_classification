import os

def update_file(row_datas,output, target_fp_name ):
    print("[INFO] Opening additional data table: %s" % (target_fp_name))
    fp2 = open(target_fp_name,'r')
    db_rows = fp2.readlines()
    fp2.close()
    

    print("[INFO] Start progress data..." )
    for row_data in row_datas:
        baby_data = row_data.split(",")
        for db_row in db_rows:
            db_data = db_row.split(",")
            if db_data[1] in baby_data[2] :            
                baby_data[4] = db_data[2]
                baby_data[5] = db_data[3].strip('\n')

        new_row = ",".join(baby_data)
        output.writelines(new_row)
    print("[INFO] Fill in data completed!\n")
    

def openFile():
    # Target file name : additional data
    target_fp_name = os.path.join(r"./simple_2/", "20201111_TB_DB.csv")

    # BA image data table
    print("[INFO] Starting fill in \"BA\" data...")
    filename_have = os.path.join(r"../Matlab/Database/simple/", "Baby_have_tb.csv")
    fp = open(filename_have,'r')
    row_datas = fp.readlines()
    fp.close()
    output = open(filename_have,'w')
    update_file(row_datas, output, target_fp_name)
    output.close()

    # none-BA image data table    
    print("[INFO] Starting fill in \"none-BA\" data...")
    filename_none = os.path.join(r"../Matlab/Database/simple/", "Baby_none_tb.csv")
    fp = open(filename_none,'r')
    row_datas = fp.readlines()
    fp.close()
    output = open(filename_none,'w')
    update_file(row_datas, output, target_fp_name)    
    output.close()

if __name__ == "__main__":
    openFile()