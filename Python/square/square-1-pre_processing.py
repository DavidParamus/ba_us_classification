import shutil
import os
import cv2

#pwd = "D:/DST/作業專區/資工系/108 碩士計畫\利用肝膽超音波評估新生兒及嬰兒之黃疸病狀"
database = "../Matlab/Database/square_20210426"

def img_rename_and_crop(image_type, target_path, crop_coor, crop_size, rewrite=False):
    print("[INFO] Rename and Crop \"{}\" image from \"{}\"... ".format(image_type, target_path))
    fp = None
    datatables = None
    img_id = 1 
    if rewrite is True:
        print("[WARNING] Rewriting data...")
        # shutil.rmtree(os.path.join(database, image_type))
        os.makedirs(os.path.join(database, image_type))
        fp = open(os.path.join(database, "Baby_{}_tb.csv".format(image_type)), "w")
        fp.write("id,type,origin filename,new filename,TB,DB,isFeed,selected,remark\n")
    else: 
        print("[INFO] Appending new data...")
        fp = open(os.path.join(database, "Baby_{}_tb.csv".format(image_type)), "r")
        datatables = fp.readlines()
        fp.close()
        img_id = int(datatables[-1].split(",")[0]) + 1 
        print("[DEBUG] Final ID: %d" % img_id)
        fp = open(os.path.join(database, "Baby_{}_tb.csv".format(image_type)), "a")
    
    # print("[DEBUG] Now path: {}".format(os.getcwd()))
    # print("[DEBUG] Target path: {}".format(target_path))
    # print("[DEBUG] Target absPath: {}".format(os.path.abspath(target_path)))
    img_folders = os.listdir(target_path)

    if rewrite != True:
        temp_folders = []
        for img_folder in img_folders:
            found = False
            for datatable in datatables:
                data = datatable.split(",")
                origin_name = data[2]
                if origin_name[:origin_name.find(os.path.sep)] == img_folder:
                    found = True
                    break
            if found == False:
                temp_folders.append(img_folder)
        img_folders = temp_folders

    for img_folder in img_folders:
        print("[INFO] Progress folder: {}".format(img_folder))
        feed_folders = os.listdir(os.path.join(target_path, img_folder))
        feed_folders.sort()
       
        # print("[DEBUG] Is \'{}\' is a folder? {}".format(os.path.join(target_path, img_folder,feed_folders[0]), os.path.isdir(os.path.join(target_path, img_folder,feed_folders[0]))))
        if os.path.isdir(os.path.join(target_path, img_folder,feed_folders[0])):
            for feed_folder in feed_folders:
                if 'feeding' in feed_folder:
                    isFeeding = 1
                else:
                    isFeeding = 0
                
                f_names = os.listdir(os.path.join(target_path, img_folder, feed_folder))
                f_names.sort()
                for fname in f_names:
                    # print("[DEBUG] Target image: %s" % (fname))
                    img = cv2.imread(os.path.join(target_path, img_folder, feed_folder, fname))
                    # rename image
                    patient_id = img_folder
                    new_fname = "%05d_%s_%s_%s_%s" % (img_id, image_type, patient_id, feed_folder, fname)
                        
                    # Crop ROI
                    img = img[ crop_coor[1]:crop_coor[1]+crop_size[1],
                               crop_coor[0]:crop_coor[0]+crop_size[0] ]

                    # Write Image
                    if not os.path.exists(os.path.join(database, image_type)):
                        os.path.join(database, image_type)
                    cv2.imwrite(os.path.join(database, image_type, new_fname),img)
            
                    # Write table
                    fp.write("%d,%s,%s,%s,%d,%d,%d,%d,\n" % (img_id, image_type, os.path.join(img_folder, fname), new_fname, 0, 0, isFeeding, 0))
                    img_id += 1
        else:
            f_names = os.listdir(os.path.join(target_path, img_folder))
            f_names.sort()
            for fname in f_names:
                # print("[DEBUG] Target image: %s" % (fname))
                img = cv2.imread(os.path.join(target_path, img_folder, fname))
                
                # rename image
                folder_split = img_folder.replace("\n","").split(' ')
                new_fname = "%05d_%s_%s_%s" % (img_id, image_type, folder_split[0], fname)
                
                # Crop ROI
                img = img[ crop_coor[1]:crop_coor[1]+crop_size[1],
                           crop_coor[0]:crop_coor[0]+crop_size[0] ]
                
                cv2.imwrite(os.path.join(database, image_type, new_fname),img)
                if len(folder_split) > 5:
                    fp.write("{},{},{},{},{},{},0,0,{}\n".format(img_id, image_type, os.path.join(img_folder, fname), new_fname, folder_split[-3], folder_split[-1], folder_split[-5]))
                elif len(folder_split) > 2:
                    fp.write("{},{},{},{},{},{},0,0,\n".format(img_id, image_type, os.path.join(img_folder, fname), new_fname, folder_split[-3], folder_split[-1]))
                else:
                    fp.write("{},{},{},{},0,0,0,0,\n".format(img_id, image_type, os.path.join(img_folder, fname), new_fname))
                img_id += 1
    fp.close()
    print("[INFO] Image rename and crop completed!")


def main():

    type1_crop_coor = (315, 212)  # 裁切起始位置
    type1_crop_size = (290, 290) # 裁切大小

    type1_1_crop_coor = (295, 170)  # 裁切起始位置
    type1_1_crop_size = (330, 330) # 裁切大小

    type2_crop_coor = (194, 139)  # 裁切起始位置
    type2_crop_size = (255, 255) # 裁切大小

    type2_1_crop_coor = (170, 120)  # 裁切起始位置
    type2_1_crop_size = (300, 300) # 裁切大小

    type3_crop_coor = (180, 160)  # 裁切起始位置
    type3_crop_size = (270, 270) # 裁切大小

    # 2020-04-09
    print("[INFO] Starting preprocessing date \"2020-04-09\" 's dataset....")   
    have_path = '../Dataset/type1/2020-04-09/Have/'
    img_rename_and_crop('have', have_path, type1_crop_coor, type1_crop_size, rewrite=True)

    none_path = '../Dataset/type1/2020-04-09/None/'
    img_rename_and_crop('none', none_path, type1_crop_coor, type1_crop_size, rewrite=True)

    # 2020-11-11
    print("[INFO] Starting preprocessing date \"2020-11-11\" 's dataset....")   
    have_path = '../Dataset/type2/2020-11-11/have/'
    img_rename_and_crop('have', have_path, type2_crop_coor, type2_crop_size)
    
    have_path = '../Dataset/type2/2020-11-11/none/'
    img_rename_and_crop('none', have_path, type2_crop_coor, type2_crop_size)

    have_path = '../Dataset/type1/2020-11-11/have/'
    img_rename_and_crop('have', have_path, type1_crop_coor, type1_crop_size)

    none_path = '../Dataset/type1/2020-11-11/none/'
    img_rename_and_crop('none', none_path, type1_crop_coor, type1_crop_size)
    
    # 2021-03-10
    print("[INFO] Starting preprocessing date \"2021-03-10\" 's dataset....")   
    have_path = '../Dataset/type2/2021-03-10/have/'
    img_rename_and_crop('have', have_path, type2_crop_coor, type2_crop_size)
    
    none_path = '../Dataset/type1/2021-03-10/none/'
    img_rename_and_crop('none', none_path, type1_crop_coor, type1_crop_size)

    
    # 2021-03-30
    print("[INFO] Starting preprocessing date \"2021-03-30\" 's dataset....")   
    none_path = '../Dataset/type1/2021-03-30/none/'
    img_rename_and_crop('none', none_path, type1_crop_coor, type1_crop_size)
    
    none_path = '../Dataset/type1_1/2021-03-30/none/'
    img_rename_and_crop('none', none_path, type1_1_crop_coor, type1_1_crop_size)

    none_path = '../Dataset/type2/2021-03-30/none/'
    img_rename_and_crop('none', none_path, type2_crop_coor, type2_crop_size)
    
    none_path = '../Dataset/type2_1/2021-03-30/none/'
    img_rename_and_crop('none', none_path, type2_1_crop_coor, type2_1_crop_size)

    have_path = '../Dataset/type3/2021-03-30/have/'
    img_rename_and_crop('have', have_path, type3_crop_coor, type3_crop_size)

    # 2021-04-15
    # ** Type4 
    have_path = '../Dataset/type4/type4_48/2021-04-15/have/'
    img_rename_and_crop('have', have_path, crop_coor=(165,53), crop_size=(370,370))

    have_path = '../Dataset/type4/type4_59/2021-04-15/have/'
    img_rename_and_crop('have', have_path, crop_coor=(179,80), crop_size=(325,325))

    have_path = '../Dataset/type4/type4_70/2021-04-15/have/'
    img_rename_and_crop('have', have_path, crop_coor=(207,100), crop_size=(269,269))

    have_path = '../Dataset/type4/type4_80/2021-04-15/have/'
    img_rename_and_crop('have', have_path, crop_coor=(189,121), crop_size=(262,262))

    # ** Type5
    have_path = '../Dataset/type5/type5_60/2021-04-15/have/'
    img_rename_and_crop('have', have_path, crop_coor=(185,80), crop_size=(268,268))

    have_path = '../Dataset/type5/type5_70/2021-04-15/have/'
    img_rename_and_crop('have', have_path, crop_coor=(185,90), crop_size=(268,268))

    have_path = '../Dataset/type5/type5_80/2021-04-15/have/'
    img_rename_and_crop('have', have_path, crop_coor=(185,100), crop_size=(268,268))

    have_path = '../Dataset/type5/type5_R0/2021-04-15/have/'
    img_rename_and_crop('have', have_path, crop_coor=(103,165), crop_size=(240,240))



    # ** Type6
    have_path = '../Dataset/type6/type6_64/2021-04-15/have/'
    img_rename_and_crop('have', have_path, crop_coor=(184,122), crop_size=(270,270))

    have_path = '../Dataset/type6/type6_70/2021-04-15/have/'
    img_rename_and_crop('have', have_path, crop_coor=(186,115), crop_size=(264,264))

    have_path = '../Dataset/type6/type6_80/2021-04-15/have/'
    img_rename_and_crop('have', have_path, crop_coor=(189,123), crop_size=(260,260))

    have_path = '../Dataset/type6/type6_87/2021-04-15/have/'
    img_rename_and_crop('have', have_path, crop_coor=(190,126), crop_size=(260,260))

    have_path = '../Dataset/type6/type6_100/2021-04-15/have/'
    img_rename_and_crop('have', have_path, crop_coor=(184,123), crop_size=(270,270))

    # ** Type7
    have_path = '../Dataset/type7/type7_70_1/2021-04-15/have/'
    img_rename_and_crop('have', have_path, crop_coor=(143,115), crop_size=(480,480))

    have_path = '../Dataset/type7/type7_80/2021-04-15/have/'
    img_rename_and_crop('have', have_path, crop_coor=(188,208), crop_size=(390,390))

    have_path = '../Dataset/type7/type7_80_1/2021-04-15/have/'
    img_rename_and_crop('have', have_path, crop_coor=(155,134), crop_size=(450,450))

    have_path = '../Dataset/type7/type7_110/2021-04-15/have/'
    img_rename_and_crop('have', have_path, crop_coor=(168,170), crop_size=(430,430))

    # ** Type8
    have_path = '../Dataset/type8/2021-04-15/have/'
    img_rename_and_crop('have', have_path, crop_coor=(194,112), crop_size=(248,248))

if __name__ == '__main__':
    main()
    