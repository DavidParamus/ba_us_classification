import os
import shutil
import numpy as np


def separate_validation_piqe(train_dir, piqe_score, validation_dir):
    print("[INFO] Separate validataion thought PIQE score : {}".format(piqe_score))
    if not os.path.exists(validation_dir):
        os.makedirs(os.path.join(validation_dir, "have"))
        os.makedirs(os.path.join(validation_dir, "none"))

    fp = open(piqe_score, "r")
    piqe_score = fp.readlines()
    fp.close()
    validations = [s for s in piqe_score if 'Validation' in s]
    for validation in validations:
        validate_arr = validation.split(",")
        target_img = validate_arr[0] 
        if "have" in target_img:
            shutil.move(os.path.join(train_dir, "have", target_img),
                        os.path.join(validation_dir, "have", target_img))
        else: 
            shutil.move(os.path.join(train_dir, "none", target_img),
                        os.path.join(validation_dir, "none", target_img))
    print("[INFO] Separate validataion complete. File count: {}".format(len(validations)))
       

if __name__ == '__main__':
    separate_validation_piqe(r"../Matlab/Dataset/simple/Train_Image_Aug/",
                             "./simple_2/piqe_scores_simple.csv", r"../Matlab/Dataset/simple/Validation_Image")
