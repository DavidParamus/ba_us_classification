import Augmentor
import os
import shutil
import random

def move_data2dataset(data_type, image_src, write_dir, isAugment=False):
    print("[INFO] Moving \"%s\" form \"%s\" to \"%s\", Augemntation: %d..." % (data_type, image_src, write_dir, isAugment))
    train_path = os.path.join(write_dir, 'Train_Image', data_type)
    if not os.path.exists(train_path):
        os.makedirs(train_path)
    test_dir =  os.path.join(write_dir, 'Test_Image', data_type)
    if not os.path.exists(test_dir):
        os.makedirs(test_dir)


    test_patient_dir = os.path.join(write_dir, 'Test_Image_patient')

    filename = "Baby_{}_tb.csv".format(data_type)
    fp = open(os.path.join(image_src, filename), "r")
    row_datas = fp.readlines()
    fp.close()

    images = os.listdir(os.path.join(image_src, data_type))
    row_datas = row_datas[1:]
    for index, row in enumerate(row_datas):
        row_spilt = row.split(",")
        # print(row_spilt)
        if row_spilt[-2] == '1':
            # print(row_spilt[-2])
            shutil.copyfile(os.path.join(image_src, data_type, images[index]), os.path.join(train_path, images[index]))
            
        elif row_spilt[-2] == '2':
            shutil.copyfile(os.path.join(image_src, data_type, images[index]), os.path.join(test_dir, images[index]))

            patient = images[index].split("_")[2]
            write_patient_dir = os.path.join(test_patient_dir, patient, data_type)
            if not os.path.exists(write_patient_dir):
                os.makedirs(write_patient_dir)
            shutil.copyfile(os.path.join(image_src, data_type, images[index]), os.path.join(write_patient_dir, images[index]))

    if isAugment:
        data_augmentation(os.path.join(train_path, data_type), "Train_image_Aug")
    print("[INFO] Moving \"%s\" data completed!" % (data_type))


def random_select_test_image(target_table, train_dir, test_percentage, test_dir):
    print("[INFO] Random selected {} patient training as test data...".format(test_percentage))

    print("[INFO] Reading data table : {}".format(target_table))
    fp = open(target_table, "r")
    row_datas = fp.readlines()
    fp.close()
    title_row = row_datas[0]
    row_datas = row_datas[1:]
   
    # Calculate patient number
    past_patients = []
    for row_data in row_datas:
        data = row_data.split(",") # Get file name
        patient = data[3].split("_")[2] # Get patient number
        if patient in past_patients:
            continue
        else:
           past_patients.append(patient)
    
    test_number = int(round(float(len(past_patients)) * (float(test_percentage)/100) + 0.01, 0))
    print("[DEBUG] Test subject numbers : {} ; round(number, 0): {}".format(len(past_patients) * (float(test_percentage)/100), test_number))
    past_patients.clear()

    i = 0
    while i < test_number:
        print("[INFO] Test patient selected: {}/{}".format(i+1, test_number))
        rand_temp = random.randint(1,len(row_datas)+1)
        data = row_datas[rand_temp].split(",") # Get file name
        patient = data[3].split("_")[2] # Get patient number
        patient_type = data[3].split("_")[1]
        print("[DEBUG] Target patient row_datas[{}] : {} ; Repeated? {}".format(rand_temp, patient, (patient in past_patients)))
        # Filter same patient
        if patient not in past_patients:
            patient_datas = [row_data for row_data in row_datas if patient in row_data and patient not in past_patients]
            for patient_data in patient_datas:
                data = patient_data.split(",")
                # print("[DEBUG] patient: {}".format(patient))
                if data[-2] != '0':
                    target_file = data[3]

                    if not os.path.exists(os.path.join(test_dir, patient_type)):
                        os.makedirs(os.path.join(test_dir, patient_type))
                    shutil.copy(os.path.join(train_dir, target_file),
                                os.path.join(test_dir, patient_type, target_file))

                    test_patient_dir = test_dir + "_patients"
                    if not os.path.exists(os.path.join(test_patient_dir, patient, patient_type)):
                        os.makedirs(os.path.join(test_patient_dir, patient, patient_type))
                    shutil.move(os.path.join(train_dir, target_file),
                                 os.path.join(test_patient_dir, patient, patient_type,target_file))

            past_patients.append(patient)
            i+=1
    
    
    print("[INFO] Get test data index...")
    selected_index = []
    test_dir = os.path.join(test_dir, patient_type)
    test_imgs = os.listdir(test_dir)
    for test_img in test_imgs:
        test_array = test_img.split("_")
        selected_index.append(int(test_array[0]))
    
    print("[INFO] Mark test data index into data table...")
    for index, row_data in enumerate(row_datas):
        try:
            data = row_data.split(",")
            data_index = int(data[0])
            for selected in selected_index:
                if data_index == selected:
                    data[-2] = '2'
                    new_row = ",".join(data)
                    #print("[DEBUG] New_row : {}".format(new_row))
                    row_datas[index] = new_row
                    break
        except:
            print("[ERROR] row data is empyt : {}".format(row_data))
    row_datas.insert(0, title_row)
    # check_table(row_datas)
    print("[INFO] Rewriting data table : {}".format(target_table))
    fp = open(target_table, "w")
    fp.writelines(row_datas)
    fp.close()
    

def data_augmentation(img_src, target_dir):
    target_num = int(len(os.listdir(img_src))) * 20
    print("[INFO] Data augmentation  to : {} ".format(target_num))
    p = Augmentor.Pipeline(img_src, target_dir)

    # Point to a directory containing ground truth data.
    # Images with the same file names will be added as ground truth data
    # and augmented in parallel to the original data.
    # Add operations to the pipeline as normal:
    p.rotate(probability=1, max_left_rotation=5, max_right_rotation=5)
    p.flip_left_right(probability=0.5)
    p.zoom_random(probability=0.5, percentage_area=0.8)
    p.flip_top_bottom(probability=0.5)
    p.sample(target_num)

def check_table(row_datas, ):
    for row_data in row_datas:
        data = row_data.split(",")
        if data[-2] == '2':
            print(row_data.replace("\n",""))


if __name__ == "__main__":
    move_data2dataset("have", r"../Matlab/Database/square_20210426", r"../Matlab/Dataset/square_20210426/")
    move_data2dataset("none", r"../Matlab/Database/square_20210426", r"../Matlab/Dataset/square_20210426/")
    random_select_test_image(os.path.join(r"../Matlab/Database/square_20210426/Baby_have_tb.csv"),
                              r"../Matlab/Dataset/square_20210426/Train_Image/have",
                              20, r"../Matlab/Dataset/square_20210426/Test_Image")
    random_select_test_image(os.path.join(r"../Matlab/Database/square_20210426/Baby_none_tb.csv"),
                               r"../Matlab/Dataset/square_20210426/Train_Image/none",
                               20, r"../Matlab/Dataset/square_20210426/Test_Image")
    data_augmentation(r"../Matlab/Dataset/square_20210426/Train_Image/have",r"../../Train_Image_Aug/have")
    data_augmentation(r"../Matlab/Dataset/square_20210426/Train_Image/none",r"../../Train_Image_Aug/none")
