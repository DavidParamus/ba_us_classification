import Augmentor
import os
import shutil
import random

def move_data2dataset(data_type, image_src, write_dir, isAugment=False):
    filename = "Baby_{}_tb.csv".format(data_type)
    fp = open(os.path.join(image_src, filename), "r")
    row_datas = fp.readlines()
    fp.close()
    images = os.listdir(os.path.join(image_src, data_type))
    row_datas = row_datas[1:]
    for index, row in enumerate(row_datas):
        row_spilt = row.split(",")
        # print(row_spilt)
        if row_spilt[-2] != '0':
            # print(row_spilt[-2])
            shutil.copyfile(os.path.join(image_src, data_type, images[index]), 
                            os.path.join(write_dir, data_type, images[index]))
    if isAugment:
        data_augmentation(os.path.join(write_dir, data_type), "Train_image_Aug")

def random_selected_test_img(target_table, train_dir, test_percent, test_dir):
    print("[INFO] Random selected {}% training as test data...".format(test_percent))
    train_imgs = os.listdir(train_dir)
    test_img_num = int(len(train_imgs) * (test_percent/100))
    test_imgs = random.sample(train_imgs, test_img_num)
    test_imgs.sort()
    for test_img in test_imgs:
        print("[INFO] Moving data \"{}\" to test_dir: {}".format(test_img, test_dir))
        shutil.move(os.path.join(train_dir, test_img),os.path.join(test_dir, test_img))
        
    
    print("[INFO] Get test data index...".format(test_percent))
    selected_index = []
    test_imgs = os.listdir(test_dir)
    for test_img in test_imgs:
        test_array = test_img.split("_")
        selected_index.append(int(test_array[0]))

    print("[INFO] Reading data table : {}".format(target_table))
    fp = open(target_table, "r")
    row_datas = fp.readlines()
    fp.close()
    title_row = row_datas[0]
    row_datas = row_datas[1:]
    for index, row_data in enumerate(row_datas):
        try:
            data = row_data.split(",")
            data_index = int(data[0])
            for selected in selected_index:
                if data_index == selected:
                    data[-2] = '2'
                    new_row = ",".join(data)
                    #print("[DEBUG] New_row : {}".format(new_row))
                    row_datas[index] = new_row
                    break
        except:
            print("[ERROR] row data is empyt : {}".format(row_data))
    row_datas.insert(0, title_row)
    # check_table(row_datas)
    print("[INFO] Rewriting data table : {}".format(target_table))
    fp = open(target_table, "w")
    fp.writelines(row_datas)
    fp.close()
    
def data_augmentation(img_src, target_dir):
    target_num = int(len(os.listdir(img_src))) * 16
    print("[INFO] Data augmentation  to : {} ".format(target_num))
    p = Augmentor.Pipeline(img_src, target_dir)

    # Point to a directory containing ground truth data.
    # Images with the same file names will be added as ground truth data
    # and augmented in parallel to the original data.
    # Add operations to the pipeline as normal:
    p.rotate(probability=1, max_left_rotation=5, max_right_rotation=5)
    p.flip_left_right(probability=0.5)
    p.zoom_random(probability=0.5, percentage_area=0.8)
    p.flip_top_bottom(probability=0.5)
    p.sample(target_num)


if __name__ == "__main__":
    # move_data2dataset("have", r"../Matlab/Database/complex", r"../Matlab/Dataset/complex/Train_Image/")
    # move_data2dataset("none", r"../Matlab/Database/complex", r"../Matlab/Dataset/complex/Train_Image/")

    # random_selected_test_img(os.path.join(r"../Matlab/Database/complex/Baby_have_tb.csv"),
    #                          r"../Matlab/Dataset/complex/Train_Image/have",
    #                          5, r"../Matlab/Dataset/complex/Test_Image/have")
    # random_selected_test_img(os.path.join(r"../Matlab/Database/complex/Baby_none_tb.csv"),
    #                          r"../Matlab/Dataset/complex/Train_Image/none",
    #                          5, r"../Matlab/Dataset/complex/Test_Image/none")

    data_augmentation(r"../Matlab/Dataset/complex/Train_Image/have",r"../Train_Image_Aug/have")
    data_augmentation(r"../Matlab/Dataset/complex/Train_Image/none",r"../Train_Image_Aug/none")