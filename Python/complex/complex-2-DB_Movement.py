import os

def mark_selected(src_dir, filename, selected_index):
    print("[INFO] Marking selected data")
    print("[INFO] Reading data table : {}".format(filename))
    fp = open(os.path.join(src_dir, filename), "r")
    row_datas = fp.readlines()
    fp.close()
    title_row = row_datas[0]
    row_datas = row_datas[1:]
    for index, row_data in enumerate(row_datas):
        try:
            data = row_data.split(",")
            data_index = int(data[0])
            for selected in selected_index:
                if data_index == selected:
                    data[-2] = '1'
                    new_row = ",".join(data)
                    #print("[DEBUG] New_row : {}".format(new_row))
                    row_datas[index] = new_row
                    break
        except:
            print("[ERROR] row data is empyt : {}".format(row_data))
    row_datas.insert(0, title_row)
    #check_table(row_datas)
    print("[INFO] Rewriting data table : {}".format(filename))
    fp = open(os.path.join(src_dir, filename), "w")
    fp.writelines(row_datas)
    fp.close()


def check_table(row_datas):
    print("[DEBUG] Check row_datas : ")
    for row_data in row_datas:
        print(row_data.replace("\n", ""))


def mark_data():
    selected_have = list(range(1, 138+1))
    remove_target = [12, 13, 25, 28, 41, 46, 52, 56,
                     58, 62, 78, 79, 82, 105, 111, 123, 124, 127]
    for target in remove_target:
        selected_have.remove(target)
    mark_selected(r"../Matlab/Database/complex/",
                  "Baby_have_tb.csv", selected_have)


    selected_have = list(range(1, 105+1))
    remove_target = [103]
    for target in remove_target:
        selected_have.remove(target)
    mark_selected(r"../Matlab/Database/complex/",
                  "Baby_none_tb.csv", selected_have)

if __name__ == "__main__":
    mark_data()
