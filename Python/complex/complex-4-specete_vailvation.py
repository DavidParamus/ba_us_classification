import os
import shutil


def separate_validation(train_dir, piqe_score, validation_dir):
    print("[INFO] Separate validataion thought PIQE score : {}".format(piqe_score))
    fp = open(piqe_score, "r")
    piqe_score = fp.readlines()
    fp.close()
    validations = [s for s in piqe_score if 'Validation' in s]
    for validation in validations:
        validate_arr = validation.split(",")
        target_img = validate_arr[0] + ".tif"
        if "have" in target_img:
            shutil.move(os.path.join(train_dir, "have", target_img),
                        os.path.join(validation_dir, "have", target_img))
        else: 
            shutil.move(os.path.join(train_dir, "none", target_img),
                        os.path.join(validation_dir, "none", target_img))
    print("[INFO] Separate validataion complete. File count: {}".format(len(validations)))

if __name__ == '__main__':
    separate_validation(r"../Matlab/Dataset/complex/Train_Image_Aug/",
                        "./piqe_scores_complex.csv", r"../Matlab/Dataset/complex/Validation_Image/")
