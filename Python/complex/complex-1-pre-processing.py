import shutil
import os
import cv2

database = "../Matlab/Database/complex"
corp_coor = (140, 70)  # 裁切起始位置
crop_size = (650, 520) # 裁切大小

def img_rename_and_crop(image_type, target_path, rewrite=False):
    print("[INFO] Rename and Crop \"{}\" image: ".format(image_type))
    fp = None
    datatables = None
    img_id = 1
    if rewrite is True:
        print("[WARNING] Rewriting data...")
        shutil.rmtree(os.path.join(database, image_type))
        os.makedirs(os.path.join(database, image_type))
        fp = open(os.path.join(database, "Baby_{}_tb.csv".format(image_type)), "w")
        fp.write("id,type,origin filename,new filename,TB,DB,isFeed,selected,remark\n")
    else: 
        print("[INFO] Appending new data...")
        fp = open(os.path.join(database, "Baby_{}_tb.csv".format(image_type)), "r")
        datatables = fp.readlines()
        fp.close()
        img_id = int(datatables[-1][0])
        fp = open(os.path.join(database, "Baby_{}_tb.csv".format(image_type)), "a")
    
    img_folders = os.listdir(target_path)
    if rewrite != True:
        temp_folders = []
        for img_folder in img_folders:
            found = False
            for datatable in datatables:
                data = datatable.split(",")
                origin_name = data[2]
                if origin_name[:origin_name.find(os.path.sep)] == img_folder:
                    found = True
                    break
            if found == False:
                temp_folders.append(img_folder)
        img_folders = temp_folders
    
    mask = cv2.imread("./mask_2.png", cv2.IMREAD_GRAYSCALE)
    for img_folder in img_folders:
        print("[INFO] Progress folder: {}".format(img_folder))

        f_names = os.listdir(os.path.join(target_path, img_folder))
        f_names.sort()
        for fname in f_names:
            img = cv2.imread(os.path.join(target_path, img_folder, fname))
            # rename image
            folder_split = img_folder.replace("\n","").split(' ')
            new_fname = "%05d_%s_%s_%s" % (img_id, image_type, folder_split[0], fname)
            # Crop ROI
            img = img[ corp_coor[1]:corp_coor[1]+crop_size[1],
                       corp_coor[0]:corp_coor[0]+crop_size[0] ]
            # Replace ROI text to black
            img_h, img_w = img.shape[:2]
            for y in range(img_h):
                for x in range(img_w):
                    if mask[y][x] <= 0:
                        img[y][x] = [0, 0, 0]
            #cv2.imshow(new_fname, img)
            #cv2.waitKey(0)
            cv2.imwrite(os.path.join(database, image_type, new_fname),img)
            if len(folder_split) > 5:
                fp.write("{},{},{},{},{},{},0,0,{}\n".format(img_id, image_type, os.path.join(img_folder, fname), new_fname, folder_split[-3], folder_split[-1], folder_split[-5]))
            else:
                fp.write("{},{},{},{},{},{},0,0,\n".format(img_id, image_type, os.path.join(img_folder, fname), new_fname, folder_split[-3], folder_split[-1]))
            img_id += 1
    fp.close()
    print("[INFO] Image rename and crop completed!")
        

def main():
    have_path = '../Dataset/Have'
    none_path = '../Dataset/None'
    img_rename_and_crop('have', have_path, True)
    img_rename_and_crop('none', none_path, True)


if '__name__' == main():
    main()
